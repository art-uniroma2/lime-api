package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.IRI;

public class UnknownSemanticModelException extends ProfilerException {

	private static final long serialVersionUID = -5309185060434078326L;

	public UnknownSemanticModelException(IRI semantiModel) {
		super("Unknown semantic model: " + semantiModel);
	}
}

package it.uniroma2.art.lime.profiler.impl;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.profiler.ProfilerOptions;

public class RDFSLexicalizationModelProfiler extends AbstractLexicalizationModelProfiler {

	public static final IRI RDFS_LEXICALIZATION_MODEL = SimpleValueFactory.getInstance()
			.createIRI("http://www.w3.org/2000/01/rdf-schema");

	@Override
	protected TupleQuery prepareQuery(ProfilerOptions options,
			LIMERepositoryConnectionWrapper metadataConnection, RepositoryConnection dataConnection,
			IRI dataGraph) {

		StringBuilder sb = new StringBuilder(
				// @formatter:off
				" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>                             \n" +
				" SELECT ?referenceDataset ?referenceDatasetUriSpace ?lang                         \n" +
				"        (COUNT(DISTINCT ?reference) as ?references)                               \n" +
				"        (COUNT(?label) as ?lexicalizations) {                                     \n" +
				"     GRAPH ?dataGraph {                                                           \n" +
				"        ?reference rdfs:label ?label .                                            \n" +
				"        BIND(LANG(?label) as ?lang)                                               \n"
				// @formatter:on
		);

		ResourceLocationUtilsInternal.appendUriSpaceLogic(options, dataGraph, sb, metadataConnection, "",
				"reference");

		sb.append(
			// @formatter:off
			"     }                                                                            \n" +
			" }                                                                                \n" +
			" GROUP BY ?referenceDataset ?referenceDatasetUriSpace ?lang                       \n"
			// @formatter:on
		);

		TupleQuery query = dataConnection.prepareTupleQuery(sb.toString());
		query.setBinding("dataGraph", dataGraph);

		return query;
	}

	@Override
	public IRI getLexicalizationModel() {
		return RDFS_LEXICALIZATION_MODEL;
	}

	@Override
	protected boolean requiresLexicon() {
		return false;
	}

}

package it.uniroma2.art.lime.profiler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexicalizationSetStatistics {
	private Resource referenceDataset;
	private Optional<Resource> lexiconDatasetHolder = Optional.empty();
	private IRI lexicalizationModel;
	private BigInteger lexicalizations;
	private BigInteger references;
	private BigInteger lexicalEntries;
	private BigDecimal avgNumOfLexicalizations;
	private BigDecimal percentage;
	private String languageTag;
	private IRI languageLexvo;
	private IRI languageLOC;

	public Resource getReferenceDataset() {
		return referenceDataset;
	}

	public void setReferenceDataset(Resource referenceDataset) {
		this.referenceDataset = referenceDataset;
	}

	public Optional<Resource> getLexiconDataset() {
		return lexiconDatasetHolder;
	}

	public void setLexiconDataset(IRI lexiconDataset) {
		this.lexiconDatasetHolder = Optional.ofNullable(lexiconDataset);
	}

	public IRI getLexicalizationModel() {
		return lexicalizationModel;
	}

	public void setLexicalizationModel(IRI lexicalizationModel) {
		this.lexicalizationModel = lexicalizationModel;
	}

	public BigInteger getLexicalizations() {
		return lexicalizations;
	}

	public void setLexicalizations(BigInteger lexicalizations) {
		this.lexicalizations = lexicalizations;
	}

	public BigInteger getReferences() {
		return references;
	}

	public void setReferences(BigInteger references) {
		this.references = references;
	}

	public BigInteger getLexicalEntries() {
		return lexicalEntries;
	}

	public void setLexicalEntries(BigInteger lexicalEntries) {
		this.lexicalEntries = lexicalEntries;
	}

	public BigDecimal getAvgNumOfLexicalizations() {
		return avgNumOfLexicalizations;
	}

	public void setAvgNumOfLexicalizations(BigDecimal avgNumOfLexicalizations) {
		this.avgNumOfLexicalizations = avgNumOfLexicalizations;
	}

	public String getLanguageTag() {
		return languageTag;
	}

	public void setLanguageTag(String languageTag) {
		this.languageTag = languageTag;
	}

	public IRI getLanguageLexvo() {
		return languageLexvo;
	}

	public void setLanguageLexvo(IRI languageLexvo) {
		this.languageLexvo = languageLexvo;
	}

	public IRI getLanguageLOC() {
		return languageLOC;
	}

	public void setLanguageLOC(IRI languageLOC) {
		this.languageLOC = languageLOC;
	}

	public void serialize(Model graph, Resource dataset) {
		ValueFactory vf = SimpleValueFactory.getInstance();

		graph.add(dataset, RDF.TYPE, LIME.LEXICALIZATION_SET);
		graph.add(dataset, LIME.LEXICALIZATION_MODEL, lexicalizationModel);

		if (referenceDataset != null) {
			graph.add(dataset, LIME.REFERENCE_DATASET, referenceDataset);
		}
		lexiconDatasetHolder
				.ifPresent(lexiconDataset -> graph.add(dataset, LIME.LEXICON_DATASET, lexiconDataset));
		graph.add(dataset, LIME.LEXICALIZATIONS, vf.createLiteral(lexicalizations));

		if (!references.equals(BigInteger.ZERO)) {
			graph.add(dataset, LIME.REFERENCES, vf.createLiteral(references));
		}

		if (lexicalEntries != null && !lexicalEntries.equals(BigInteger.ZERO)) {
			graph.add(dataset, LIME.LEXICAL_ENTRIES, vf.createLiteral(lexicalEntries));
		}

		if (avgNumOfLexicalizations != null) {
			graph.add(dataset, LIME.AVG_NUM_OF_LEXICALIZATIONS, vf.createLiteral(avgNumOfLexicalizations));
		}

		if (percentage != null) {
			graph.add(dataset, LIME.PERCENTAGE, vf.createLiteral(percentage));
		}

		if (languageTag != null && !languageTag.isEmpty()) {
			graph.add(dataset, LIME.LANGUAGE, vf.createLiteral(languageTag));
		}

		if (languageLexvo != null) {
			graph.add(dataset, DCTERMS.LANGUAGE, languageLexvo);
		}

		if (languageLOC != null) {
			graph.add(dataset, DCTERMS.LANGUAGE, languageLOC);
		}
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public static LexicalizationSetStatistics of(Resource referenceDataset, Resource lexiconDataset,
			IRI lexicalizationModel, BigInteger lexicalizations, BigInteger references,
			BigInteger lexicalEntries, BigDecimal avgNumOfLexicalizations, BigDecimal percentage,
			String languageTag, IRI languageLexvo, IRI languageLOC) {
		LexicalizationSetStatistics stats = new LexicalizationSetStatistics();
		stats.setReferenceDataset(referenceDataset);
		stats.setLexiconDataset((IRI) lexiconDataset);
		stats.setLexicalizationModel(lexicalizationModel);
		stats.setLexicalizations(lexicalizations);
		stats.setReferences(references);
		stats.setLexicalEntries(lexicalEntries);
		stats.setAvgNumOfLexicalizations(avgNumOfLexicalizations);
		stats.setPercentage(percentage);
		stats.setLanguageTag(languageTag);
		stats.setLanguageLexvo(languageLexvo);
		stats.setLanguageLOC(languageLOC);
		return stats;
	}
}

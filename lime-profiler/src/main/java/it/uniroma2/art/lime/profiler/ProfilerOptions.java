package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.IRI;

public class ProfilerOptions {
	public static enum ResourceGenerationMode {
		UUID, BNODE, IRI
	};
	
	private boolean includeInferred;
	private IRI[] contexts;
	private boolean defaultToLocalReference;
	private ResourceGenerationMode resourceGenerationMode;
	private String mainDatasetName;
	
	public ProfilerOptions() {
		this.includeInferred = false;
		this.contexts = new IRI[0];
		this.defaultToLocalReference = false;
		this.resourceGenerationMode = ResourceGenerationMode.IRI;
	}

	public boolean isIncludeInferred() {
		return includeInferred;
	}

	public void setIncludeInferred(boolean includeInferred) {
		this.includeInferred = includeInferred;
	}

	public IRI[] getContexts() {
		return contexts;
	}

	public void setContexts(IRI[] contexts) {
		this.contexts = contexts;
	}

	public boolean isDefaultToLocalReference() {
		return defaultToLocalReference;
	}

	public void setDefaultToLocalReference(boolean defaultToLocalReference) {
		this.defaultToLocalReference = defaultToLocalReference;
	}

	public ResourceGenerationMode getResourceGenerationMode() {
		return resourceGenerationMode;
	}

	public void setResourceGenerationMode(ResourceGenerationMode resourceGenerationMode) {
		this.resourceGenerationMode = resourceGenerationMode;
	}
	
	public String getMainDatasetName() {
		return mainDatasetName;
	}
	
	public void setMainDatasetName(String mainDatasetName) {
		this.mainDatasetName = mainDatasetName;
	}
}

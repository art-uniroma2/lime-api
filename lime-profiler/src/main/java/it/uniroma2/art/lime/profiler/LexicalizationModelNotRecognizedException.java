package it.uniroma2.art.lime.profiler;

public class LexicalizationModelNotRecognizedException extends ProfilerException {

	private static final long serialVersionUID = -854404186205310169L;

	public LexicalizationModelNotRecognizedException() {
		super("Could not recognize a known lexicalizaton model");
	}
}

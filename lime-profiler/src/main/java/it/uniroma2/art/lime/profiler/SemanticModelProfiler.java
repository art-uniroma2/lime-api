package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;

public interface SemanticModelProfiler {
	ReferenceDatasetStatistics profile(RepositoryConnection conn, IRI[] graphs) throws ProfilerException;

	boolean profile(ProfilerContext profilerContext, LIMERepositoryConnectionWrapper metadataConn, RepositoryConnection dataConn,
			Resource datagraph, Resource mainDataset) throws ProfilerException;
}

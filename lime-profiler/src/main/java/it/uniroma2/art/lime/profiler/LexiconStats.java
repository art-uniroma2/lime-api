package it.uniroma2.art.lime.profiler;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexiconStats {

	private String languageTag;
	private IRI languageLexvo;
	private IRI languageLOC;
	private IRI linguisticCatalog;
	private BigInteger lexicalEntries;
	private boolean entriesExplicit;

	public String getLanguageTag() {
		return languageTag;
	}

	public void setLanguageTag(String languageTag) {
		this.languageTag = languageTag;
	}

	public IRI getLanguageLexvo() {
		return languageLexvo;
	}

	public void setLanguageLexvo(IRI languageLexvo) {
		this.languageLexvo = languageLexvo;
	}

	public IRI getLanguageLOC() {
		return languageLOC;
	}

	public void setLanguageLOC(IRI languageLOC) {
		this.languageLOC = languageLOC;
	}

	public IRI getLinguisticCatalog() {
		return linguisticCatalog;
	}

	public void setLinguisticCatalog(IRI linguisticModel) {
		this.linguisticCatalog = linguisticModel;
	}

	public void setLexicalEntries(BigInteger lexicalEntries) {
		this.lexicalEntries = lexicalEntries;
	}

	public BigInteger getLexicalEntries() {
		return lexicalEntries;
	}

	public boolean isEntriesExplicit() {
		return entriesExplicit;
	}

	public void setEntriesExplicit(boolean entriesExplicit) {
		this.entriesExplicit = entriesExplicit;
	}

	public void serialize(Model graph, Resource dataset) {
		ValueFactory vf = SimpleValueFactory.getInstance();

		graph.add(dataset, RDF.TYPE, LIME.LEXICON);
		if (linguisticCatalog != null) {
			graph.add(dataset, LIME.LINGUISTIC_CATALOG, linguisticCatalog);
		}
		if (lexicalEntries != null) {
			graph.add(dataset, LIME.LEXICAL_ENTRIES, vf.createLiteral(lexicalEntries));
		}

		if (languageTag != null && !languageTag.isEmpty()) {
			graph.add(dataset, LIME.LANGUAGE, vf.createLiteral(languageTag));
		}

		if (languageLexvo != null) {
			graph.add(dataset, DCTERMS.LANGUAGE, languageLexvo);
		}

		if (languageLOC != null) {
			graph.add(dataset, DCTERMS.LANGUAGE, languageLOC);
		}
	}

	public void parse(Model model, IRI lexicon) throws NullPointerException {
		setLanguageTag(Models.getPropertyLiteral(model, lexicon, LIME.LANGUAGE).map(Value::stringValue)
				.orElseThrow(() -> new NullPointerException()));
		setLexicalEntries(Models.getPropertyLiteral(model, lexicon, LIME.LEXICAL_ENTRIES)
				.map(l -> Literals.getIntegerValue(l, BigInteger.ZERO))
				.orElseThrow(() -> new NullPointerException()));
	}

	public static LexiconStats of(String languageTag, IRI languageLexvo, IRI languageLOC,
			IRI linguisticCatalog, BigInteger lexicalEntries) {
		LexiconStats stats = new LexiconStats();
		stats.setLanguageTag(languageTag);
		stats.setLanguageLexvo(languageLexvo);
		stats.setLanguageLOC(languageLOC);
		stats.setLinguisticCatalog(linguisticCatalog);
		stats.setLexicalEntries(lexicalEntries);
		return stats;
	}

}

/* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Portions created by ART Group, University of Rome Tor Vergata are Copyright (C) 2013 */

package it.uniroma2.art.lime.profiler;

/**
 * An exception thrown during the profiling of an RDF dataset for the generation of its LIME description.
 */
public class ProfilerException extends Exception {

	private static final long serialVersionUID = -4592903604538699942L;

	/**
	 * Constructs an exception with <code>null</code> as its detail message. The cause is not initialized, and
	 * may subsequently be initialized by a call to {@link Throwable#initCause(Throwable)}.
	 */
	public ProfilerException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param message
	 * @param cause
	 *            may be <code>null</code>, to indicate that the cause is unknown or nonexistent
	 */
	public ProfilerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs an exception with the specified detail message. The cause is not initialized, and may
	 * subsequently be initialized by a call to {@link Throwable#initCause(Throwable)}. * @param message
	 */
	public ProfilerException(String message) {
		super(message);
	}

	/**
	 * Constructs an exception with the specified cause and a detail message of (cause == null ? null :
	 * cause.toString()).
	 * 
	 * @param cause a null value is permitted, and indicates that the cause in nonexistent or unknown
	 */
	public ProfilerException(Throwable cause) {
		super(cause);
	}

}

package it.uniroma2.art.lime.profiler.impl;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.lime.model.vocabulary.VOAF;
import it.uniroma2.art.lime.profiler.ReferenceDatasetStatistics;

public class OWLReferenceDatasetStatistics extends ReferenceDatasetStatistics {
	private BigInteger classNumber;
	private BigInteger propertyNumber;

	public BigInteger getClassNumber() {
		return classNumber;
	}

	public void setClassNumber(BigInteger classNumber) {
		this.classNumber = classNumber;
	}

	public BigInteger getPropertyNumber() {
		return propertyNumber;
	}

	public void setPropertyNumber(BigInteger propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	
	@Override
	public void serialize(Model graph, Resource dataset) {
		super.serialize(graph, dataset);
		
		ValueFactory vf = SimpleValueFactory.getInstance();
		
		graph.add(dataset, RDF.TYPE, VOAF.VOCABULARY);
		graph.add(dataset, VOAF.CLASS_NUMBER, vf.createLiteral(classNumber));
		graph.add(dataset, VOAF.PROPERTY_NUMBER, vf.createLiteral(propertyNumber));
	}

}

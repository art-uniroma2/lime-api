package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.Resource;

public class MissingLexiconException extends ProfilerException {

	private static final long serialVersionUID = 8040927607236831838L;

	public MissingLexiconException(Resource dataset) {
		super("Dataset missing a lexicon: " + dataset.stringValue());
	}
}

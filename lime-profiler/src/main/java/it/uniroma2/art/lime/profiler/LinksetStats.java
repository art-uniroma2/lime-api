package it.uniroma2.art.lime.profiler;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.VOID;

public class LinksetStats {
	private Resource subjectsTarget;
	private Resource objectsTarget;
	private BigInteger triples;
	private IRI linkPredicate;

	public Resource getSubjectsTarget() {
		return subjectsTarget;
	}

	public void setSubjectsTarget(Resource subjectsTarget) {
		this.subjectsTarget = subjectsTarget;
	}

	public Resource getObjectsTarget() {
		return objectsTarget;
	}

	public void setObjectsTarget(Resource objectsTarget) {
		this.objectsTarget = objectsTarget;
	}

	public BigInteger getTriples() {
		return triples;
	}

	public void setTriples(BigInteger triples) {
		this.triples = triples;
	}

	public IRI getLinkPredicate() {
		return linkPredicate;
	}

	public void setLinkPredicate(IRI linkPredicate) {
		this.linkPredicate = linkPredicate;
	}

	public void serialize(Model graph, Resource dataset) {
		SimpleValueFactory vf = SimpleValueFactory.getInstance();

		graph.add(dataset, RDF.TYPE, VOID.LINKSET);

		if (subjectsTarget != null) {
			graph.add(dataset, VOID.SUBJECTS_TARGET, subjectsTarget);
		}

		if (objectsTarget != null) {
			graph.add(dataset, VOID.OBJECTS_TARGET, objectsTarget);
		}

		if (triples != null) {
			graph.add(dataset, VOID.TRIPLES, vf.createLiteral(triples));
		}

		if (linkPredicate != null) {
			graph.add(dataset, VOID.LINK_PREDICATE, linkPredicate);
		}
	}

}

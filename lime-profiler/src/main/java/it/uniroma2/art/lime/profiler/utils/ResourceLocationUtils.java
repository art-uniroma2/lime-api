package it.uniroma2.art.lime.profiler.utils;

import org.eclipse.rdf4j.model.IRI;

import java.util.regex.Pattern;

/**
 * Utility class for the localization of the resources.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class ResourceLocationUtils {
    /**
     * This {@link Pattern} analyzes the IRI of a <em>mentioned resource<em> extracting its namespace (group 1).
     *
     * This pattern assumes that the provided IRI denotes a resource (rather an ontology on its own); therefore, an
     * eventual trailing slash (<code>https://sws.geonames.org/102358/</code>) is simply discarded.
     */
    public static final Pattern RESOURCE_MENTION_NAMESPACE_PATTERN = Pattern.compile("^(.+#|.+[/:]).+$");
    public static final int NS_GROUP_INDEX = 1;

    /**
     * Guesses the namespace for a resource mention
     *
     * @param resource
     * @return
     * @see #RESOURCE_MENTION_NAMESPACE_PATTERN
     */
    public static String guessNamespaceForResourceMention(IRI resource) {
        var matcher = RESOURCE_MENTION_NAMESPACE_PATTERN.matcher(resource.stringValue());
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Unable to guess the namespace for %s".formatted(resource));
        }
        return matcher.group(NS_GROUP_INDEX);
    }

}

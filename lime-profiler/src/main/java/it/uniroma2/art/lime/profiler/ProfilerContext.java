package it.uniroma2.art.lime.profiler;

import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.profiler.ProfilerOptions.ResourceGenerationMode;

public class ProfilerContext {
	private ProfilerOptions options;

	private IRI metadataBaseURI;

	private LIMERepositoryConnectionWrapper metadataConnection;

	public ProfilerOptions getOptions() {
		return options;
	}

	public void setOptions(ProfilerOptions options) {
		this.options = options;
	}

	public IRI getMetadataBaseURI() {
		return metadataBaseURI;
	}

	public void setMetadataBaseURI(IRI metadataBaseURI) {
		this.metadataBaseURI = metadataBaseURI;
	}

	public LIMERepositoryConnectionWrapper getMetadataConnection() {
		return metadataConnection;
	}

	public void setMetadataConnection(LIMERepositoryConnectionWrapper metadataConnection) {
		this.metadataConnection = metadataConnection;
	}

	public Resource mintDatasetResource() {
		switch (options.getResourceGenerationMode()) {
		case UUID:
		case IRI: {
			String ns;
			if (metadataBaseURI.stringValue().endsWith("/")) {
				ns = metadataBaseURI.stringValue();
			} else {
				ns = metadataBaseURI.stringValue() + "#";
			}
			return metadataConnection.getValueFactory().createIRI(ns, UUID.randomUUID().toString());
		}
		case BNODE: {
			return metadataConnection.getValueFactory().createBNode();
		}
		default:
			throw new IllegalStateException(
					"Unsupported resource generation mode: " + options.getResourceGenerationMode());
		}
	}

	public Resource mintMainDatasetResource() {
		if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
			String ns;
			if (metadataBaseURI.stringValue().endsWith("/")) {
				ns = metadataBaseURI.stringValue();
			} else {
				ns = metadataBaseURI.stringValue() + "#";
			}

			String mainDatasetName = options.getMainDatasetName();

			if (mainDatasetName != null) {
				return metadataConnection.getValueFactory().createIRI(ns, mainDatasetName);
			} else {
				return metadataConnection.getValueFactory().createIRI(ns, UUID.randomUUID().toString());

			}
		} else {
			return mintDatasetResource();
		}
	}

	public Resource mintLexicalizationSetResource(Resource referenceDataset, Resource lexiconDataset,
			String languageTag) {
		if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
			if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
				String ns;
				if (metadataBaseURI.stringValue().endsWith("/")) {
					ns = metadataBaseURI.stringValue();
				} else {
					ns = metadataBaseURI.stringValue() + "#";
				}

				StringBuilder localNameBuilder = new StringBuilder();
				localNameBuilder.append(((IRI) referenceDataset).getLocalName());

				if (lexiconDataset != null) {
					localNameBuilder.append("_").append(((IRI) lexiconDataset).getLocalName());
				}

				localNameBuilder.append("_").append(languageTag).append("_lexicalization_set");

				return metadataConnection.getValueFactory().createIRI(ns, localNameBuilder.toString());
			} else {
				return mintDatasetResource();
			}

		} else {
			return mintDatasetResource();
		}
	}

	public Resource mintConceptualizationSetResource(Resource conceptSet, Resource lexiconDataset) {
		if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
			if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
				String ns;
				if (metadataBaseURI.stringValue().endsWith("/")) {
					ns = metadataBaseURI.stringValue();
				} else {
					ns = metadataBaseURI.stringValue() + "#";
				}

				StringBuilder localNameBuilder = new StringBuilder();
				localNameBuilder.append(((IRI) lexiconDataset).getLocalName());
				localNameBuilder.append("_").append(((IRI)conceptSet).getLocalName()).append("_conceptualization_set");

				return metadataConnection.getValueFactory().createIRI(ns, localNameBuilder.toString());
			} else {
				return mintDatasetResource();
			}

		} else {
			return mintDatasetResource();
		}
	}

	public Resource mintLinksetResource(Resource subjectsTarget, Resource objectsTarget, IRI linkPredicate) {
		if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
			if (options.getResourceGenerationMode() == ResourceGenerationMode.IRI) {
				String ns;
				if (metadataBaseURI.stringValue().endsWith("/")) {
					ns = metadataBaseURI.stringValue();
				} else {
					ns = metadataBaseURI.stringValue() + "#";
				}

				StringBuilder localNameBuilder = new StringBuilder();
				localNameBuilder.append(((IRI) subjectsTarget).getLocalName());
				localNameBuilder.append("_").append(((IRI) objectsTarget).getLocalName());

				if (linkPredicate != null) {
					localNameBuilder.append("_").append(linkPredicate.getLocalName());
				}

				localNameBuilder.append("_linkset");

				return metadataConnection.getValueFactory().createIRI(ns, localNameBuilder.toString());
			} else {
				return mintDatasetResource();
			}

		} else {
			return mintDatasetResource();
		}
	}

}

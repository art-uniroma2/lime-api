package it.uniroma2.art.lime.profiler;

import java.util.Collection;
import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;

public interface LexicalizationModelProfiler {
	public Collection<LexicalizationSetStatistics> profile(RepositoryConnection conn, IRI[] graphs,
			IRI referenceDataset, ReferenceDatasetStatistics referenceDatasetStats,
			Map<IRI, LexiconStats> lexiconStats) throws ProfilerException;

	boolean profile(ProfilerContext profilerContext, LIMERepositoryConnectionWrapper metadataConnection,
			RepositoryConnection dataConnection, IRI datagGraph, Resource dataset) throws ProfilerException;
}

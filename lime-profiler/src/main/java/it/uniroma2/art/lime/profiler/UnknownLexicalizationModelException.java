package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.IRI;

public class UnknownLexicalizationModelException extends ProfilerException {

	private static final long serialVersionUID = -7351007321351145391L;

	public UnknownLexicalizationModelException(IRI lexicalizationModel) {
		super("Unknown lexicalization model: " + lexicalizationModel);
	}
}

package it.uniroma2.art.lime.profiler;

public class SemanticModelNotRecognizedException extends ProfilerException {

	private static final long serialVersionUID = 4662465843727247470L;

	public SemanticModelNotRecognizedException() {
		super("Could not recognize a known semantic model");
	}
}

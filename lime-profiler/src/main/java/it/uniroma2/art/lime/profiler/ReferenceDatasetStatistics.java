package it.uniroma2.art.lime.profiler;

import java.math.BigInteger;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.VOID;

import com.google.common.base.MoreObjects;

public class ReferenceDatasetStatistics {
	private String uriSpace;
	private BigInteger triples;
	private BigInteger entities;
	private Set<IRI> conformance;

	public String getUriSpace() {
		return uriSpace;
	}

	public void setUriSpace(String uriSpace) {
		this.uriSpace = uriSpace;
	}

	public BigInteger getTriples() {
		return triples;
	}

	public void setTriples(BigInteger triples) {
		this.triples = triples;
	}

	public BigInteger getEntities() {
		return entities;
	}

	public void setEntities(BigInteger entities) {
		this.entities = entities;
	}

	public Set<IRI> getConformance() {
		return conformance;
	}

	public void setConformance(Set<IRI> conformance) {
		this.conformance = Collections.unmodifiableSet(new HashSet<IRI>(conformance));
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("uriSpace", uriSpace).add("triples", triples)
				.add("entities", entities).add("conformance", conformance).toString();
	}

	public void serialize(Model graph, Resource dataset) {
		ValueFactory vf = SimpleValueFactory.getInstance();

		graph.add(dataset, RDF.TYPE, VOID.DATASET);

		if (uriSpace != null) {
			graph.add(dataset, VOID.URI_SPACE, vf.createLiteral(uriSpace));
		}

		for (IRI iri : conformance) {
			graph.add(dataset, DCTERMS.CONFORMS_TO, iri);
		}

		if (triples != null) {
			graph.add(dataset, VOID.TRIPLES, vf.createLiteral(triples));
		}
		graph.add(dataset, VOID.ENTITIES, vf.createLiteral(entities));
	}
}

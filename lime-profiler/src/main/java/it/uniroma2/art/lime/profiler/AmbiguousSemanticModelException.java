package it.uniroma2.art.lime.profiler;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;

import com.google.common.collect.ImmutableSet;

public class AmbiguousSemanticModelException extends ProfilerException {
	private static final long serialVersionUID = -6345684491780265658L;

	private Resource dataset;
	private Set<IRI> semanticModels;

	public AmbiguousSemanticModelException(Resource dataset, Set<IRI> semanticModels) {
		super("Dataset \"" + dataset + "\" has ambiguous semantic models: " + semanticModels);
		this.dataset = dataset;
		this.semanticModels = ImmutableSet.copyOf(semanticModels);
	}

	public Resource getDataset() {
		return dataset;
	}

	public Set<IRI> getSemanticModels() {
		return semanticModels;
	}
}

package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.Resource;

public class MissingReferenceDatasetException extends ProfilerException {

	private static final long serialVersionUID = 482427263379997546L;

	public MissingReferenceDatasetException(Resource dataset) {
		super("Dataset missing a reference dataset: " + dataset.stringValue());
	}
}

package it.uniroma2.art.lime.profiler.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.impl.SimpleDataset;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import com.google.common.collect.Sets;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.profiler.ProfilerContext;
import it.uniroma2.art.lime.profiler.ProfilerException;
import it.uniroma2.art.lime.profiler.ReferenceDatasetStatistics;
import it.uniroma2.art.lime.profiler.SemanticModelProfiler;

public class SKOSSemanticModelProfiler implements SemanticModelProfiler {

	@Override
	public ReferenceDatasetStatistics profile(RepositoryConnection conn, IRI[] graphs)
			throws ProfilerException {
		long triples = conn.size(graphs);

		SimpleDataset dataset = new SimpleDataset();
		Arrays.stream(graphs).forEach(dataset::addDefaultGraph);

		String queryString =
				// @formatter:off
				" prefix owl: <http://www.w3.org/2002/07/owl#>                          \n" +
				" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>                  \n" +
	            "                                                                       \n" +
				" select (count(distinct ?resource) as ?c) {                            \n" +
				" 	?metaclass rdfs:subClassOf* ?cls .                                  \n" +
				" 	?resource a ?metaclass .                                            \n" +
				" }                                                                     \n" +
				" VALUES(?cls){%PLACEHOLDER%}\n"
				// @formatter:on
		;

		TupleQuery conceptCounter = conn.prepareTupleQuery(
				queryString.replace("%PLACEHOLDER%", "(" + RenderUtils.toSPARQL(SKOS.CONCEPT) + ")"));

		conceptCounter.setDataset(dataset);
		conceptCounter.setIncludeInferred(false);

		BigInteger conceptCount = Literals.getIntegerValue(
				QueryResults.singleResult(conceptCounter.evaluate()).getValue("c"), BigInteger.ZERO);

		String replacement = (String) Arrays.asList(SKOS.COLLECTION, SKOS.ORDERED_COLLECTION).stream()
				.map(r -> "(" + RenderUtils.toSPARQL(r) + ")").collect(Collectors.joining(""));
		TupleQuery collectionCounter = conn
				.prepareTupleQuery(queryString.replace("%PLACEHOLDER%", replacement));

		collectionCounter.setDataset(dataset);
		collectionCounter.setIncludeInferred(false);

		BigInteger collectionCount = Literals.getIntegerValue(
				QueryResults.singleResult(collectionCounter.evaluate()).getValue("c"), BigInteger.ZERO);

		TupleQuery conceptSchemeCounter = conn.prepareTupleQuery(
				queryString.replace("%PLACEHOLDER%", "(" + RenderUtils.toSPARQL(SKOS.CONCEPT_SCHEME) + ")"));

		conceptSchemeCounter.setDataset(dataset);
		conceptSchemeCounter.setIncludeInferred(false);

		BigInteger conceptSchemeCount = Literals.getIntegerValue(
				QueryResults.singleResult(conceptSchemeCounter.evaluate()).getValue("c"), BigInteger.ZERO);

		
		SKOSReferenceDatasetStatistics stats = new SKOSReferenceDatasetStatistics();
		stats.setTriples(BigInteger.valueOf(triples));
		stats.setConformance(Sets.newHashSet(
				SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core")));
		stats.setEntities(conceptCount.add(collectionCount).add(conceptSchemeCount));
		stats.setConceptNumber(conceptCount);
		stats.setCollectionNumber(collectionCount);
		stats.setConceptSchemeNumber(conceptSchemeCount);

		return stats;
	}

	@Override
	public boolean profile(ProfilerContext profilerContext, LIMERepositoryConnectionWrapper metadataConn,
			RepositoryConnection dataConn, Resource dataGraph, Resource mainDataset) {
		IRI[] graphs = profilerContext.getOptions().getContexts();
		boolean includeInferred = profilerContext.getOptions().isIncludeInferred();
		
		SimpleDataset dataset = new SimpleDataset();
		Arrays.stream(graphs).forEach(dataset::addDefaultGraph);

		String queryString =
				// @formatter:off
				" prefix owl: <http://www.w3.org/2002/07/owl#>                          \n" +
				" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>                  \n" +
	            "                                                                       \n" +
				" select (count(distinct ?resource) as ?c) {                            \n" +
				" 	?metaclass rdfs:subClassOf* ?cls .                                  \n" +
				" 	?resource a ?metaclass .                                            \n" +
				" }                                                                     \n" +
				" VALUES(?cls){%PLACEHOLDER%}\n"
				// @formatter:on
		;

		TupleQuery conceptCounter = dataConn.prepareTupleQuery(
				queryString.replace("%PLACEHOLDER%", "(" + RenderUtils.toSPARQL(SKOS.CONCEPT) + ")"));

		conceptCounter.setDataset(dataset);
		conceptCounter.setIncludeInferred(includeInferred);

		BigInteger conceptCount = Literals.getIntegerValue(
				QueryResults.singleResult(conceptCounter.evaluate()).getValue("c"), BigInteger.ZERO);

		String replacement = (String) Arrays.asList(SKOS.COLLECTION, SKOS.ORDERED_COLLECTION).stream()
				.map(r -> "(" + RenderUtils.toSPARQL(r) + ")").collect(Collectors.joining(""));
		TupleQuery collectionCounter = dataConn
				.prepareTupleQuery(queryString.replace("%PLACEHOLDER%", replacement));

		collectionCounter.setDataset(dataset);
		collectionCounter.setIncludeInferred(includeInferred);

		BigInteger collectionCount = Literals.getIntegerValue(
				QueryResults.singleResult(collectionCounter.evaluate()).getValue("c"), BigInteger.ZERO);
		
		TupleQuery conceptSchemeCounter = dataConn.prepareTupleQuery(
				queryString.replace("%PLACEHOLDER%", "(" + RenderUtils.toSPARQL(SKOS.CONCEPT_SCHEME) + ")"));

		conceptSchemeCounter.setDataset(dataset);
		conceptSchemeCounter.setIncludeInferred(false);

		BigInteger conceptSchemeCount = Literals.getIntegerValue(
				QueryResults.singleResult(conceptSchemeCounter.evaluate()).getValue("c"), BigInteger.ZERO);

		
		BigInteger entities = conceptCount.add(collectionCount).add(conceptSchemeCount);

		if (entities.equals(BigInteger.ZERO)) {
			return false;
		}

		SKOSReferenceDatasetStatistics stats = new SKOSReferenceDatasetStatistics();
		stats.setConformance(Sets.newHashSet(
				SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core")));
		stats.setEntities(entities);
		stats.setConceptNumber(conceptCount);
		stats.setCollectionNumber(collectionCount);
		stats.setConceptSchemeNumber(conceptSchemeCount);

		Model graph = new LinkedHashModel();
		stats.serialize(graph, mainDataset);

		metadataConn.add(graph);

		return true;
	}
}

package it.uniroma2.art.lime.profiler;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.VOID;

import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;

public class ConceptSetStats {
	private BigInteger concepts;
	private boolean entriesExplicit;
	private String uriSpace;

	public BigInteger getConcepts() {
		return concepts;
	}

	public void setConcepts(BigInteger concepts) {
		this.concepts = concepts;
	}

	public void serialize(Model graph, Resource dataset) {
		ValueFactory vf = SimpleValueFactory.getInstance();

		graph.add(dataset, RDF.TYPE, ONTOLEX.CONCEPT_SET);
		if (concepts != null) {
			graph.add(dataset, LIME.CONCEPTS, vf.createLiteral(concepts));
		}

		if (uriSpace != null) {
			graph.add(dataset, VOID.URI_SPACE, vf.createLiteral(uriSpace));
		}
	}

	public void setEntriesExplicit(boolean entriesExplicit) {
		this.entriesExplicit = entriesExplicit;
	}

	public boolean isEntriesExplicit() {
		return entriesExplicit;
	}

	public void setUriSpace(String uriSpace) {
		this.uriSpace = uriSpace;
	}

	public String getUriSpace() {
		return uriSpace;
	}

	public void parse(Model model, IRI conceptSet) throws NullPointerException {
		setConcepts(Models.getProperty(model, conceptSet, LIME.CONCEPTS)
				.map(l -> Literals.getIntegerValue(l, BigInteger.ZERO))
				.orElseThrow(() -> new NullPointerException()));
	}

	public static ConceptSetStats of(BigInteger concepts) {
		ConceptSetStats stats = new ConceptSetStats();
		stats.setConcepts(concepts);
		return stats;
	}

}

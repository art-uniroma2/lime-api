package it.uniroma2.art.lime.profiler;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.VOID;

import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexicalLinksetStats {
	private IRI referenceDataset;
	private IRI conceptualDataset;
	private BigInteger links;
	private BigInteger references;
	private BigInteger concepts;
	private BigDecimal avgNumOfLinks;
	private IRI linkPredicate;

	public IRI getReferenceDataset() {
		return referenceDataset;
	}

	public void setReferenceDataset(IRI referenceDataset) {
		this.referenceDataset = referenceDataset;
	}

	public IRI getConceptualDataset() {
		return conceptualDataset;
	}

	public void setConceptualDataset(IRI conceptualDataset) {
		this.conceptualDataset = conceptualDataset;
	}

	public BigInteger getLinks() {
		return links;
	}

	public void setLinks(BigInteger links) {
		this.links = links;
	}

	public BigInteger getReferences() {
		return references;
	}

	public void setReferences(BigInteger references) {
		this.references = references;
	}

	public BigInteger getConcepts() {
		return concepts;
	}

	public void setConcepts(BigInteger concepts) {
		this.concepts = concepts;
	}

	public BigDecimal getAvgNumOfLinks() {
		return avgNumOfLinks;
	}

	public void setAvgNumOfLinks(BigDecimal avgNumOfLinks) {
		this.avgNumOfLinks = avgNumOfLinks;
	}

	public IRI getLinkPredicate() {
		return linkPredicate;
	}

	public void setLinkPredicate(IRI linkPredicate) {
		this.linkPredicate = linkPredicate;
	}

	public void serialize(Model graph, Resource dataset) {
		SimpleValueFactory vf = SimpleValueFactory.getInstance();

		graph.add(dataset, RDF.TYPE, VOID.LINKSET);
		graph.add(dataset, RDF.TYPE, LIME.LEXICAL_LINKSET);

		if (referenceDataset != null) {
			graph.add(dataset, LIME.REFERENCE_DATASET, referenceDataset);
		}

		if (conceptualDataset != null) {
			graph.add(dataset, LIME.CONCEPTUAL_DATASET, conceptualDataset);
		}

		if (links != null) {
			graph.add(dataset, LIME.LINKS, vf.createLiteral(links));
		}

		if (references != null) {
			graph.add(dataset, LIME.REFERENCES, vf.createLiteral(references));
		}

		if (concepts != null) {
			graph.add(dataset, LIME.CONCEPTS, vf.createLiteral(concepts));
		}

		if (avgNumOfLinks != null) {
			graph.add(dataset, LIME.AVG_NUM_OF_LINKS, vf.createLiteral(avgNumOfLinks));
		}

		if (linkPredicate != null) {
			graph.add(dataset, VOID.LINK_PREDICATE, linkPredicate);
		}
	}

}

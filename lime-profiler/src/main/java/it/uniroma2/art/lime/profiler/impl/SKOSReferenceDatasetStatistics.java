package it.uniroma2.art.lime.profiler.impl;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.VOID;

import it.uniroma2.art.lime.profiler.ReferenceDatasetStatistics;

public class SKOSReferenceDatasetStatistics extends ReferenceDatasetStatistics {
	private BigInteger conceptNumber;
	private BigInteger collectionNumber;
	private BigInteger conceptSchemeNumber;

	public BigInteger getConceptNumber() {
		return conceptNumber;
	}

	public void setConceptNumber(BigInteger conceptNumber) {
		this.conceptNumber = conceptNumber;
	}

	public BigInteger getCollectionNumber() {
		return collectionNumber;
	}

	public void setCollectionNumber(BigInteger collectionNumber) {
		this.collectionNumber = collectionNumber;
	}
	
	public void setConceptSchemeNumber(BigInteger conceptSchemeNumber) {
		this.conceptSchemeNumber = conceptSchemeNumber;
	}

	@Override
	public void serialize(Model graph, Resource dataset) {
		super.serialize(graph, dataset);

		ValueFactory vf = SimpleValueFactory.getInstance();

		BNode conceptPartition = vf.createBNode();
		BNode collectionPartition = vf.createBNode();
		BNode conceptSchemePartition = vf.createBNode();
		
		graph.add(dataset, VOID.CLASS_PARTITION, conceptPartition);
		graph.add(dataset, VOID.CLASS_PARTITION, collectionPartition);
		graph.add(dataset, VOID.CLASS_PARTITION, conceptSchemePartition);
		graph.add(conceptPartition, VOID.CLASS, SKOS.CONCEPT);
		graph.add(conceptPartition, VOID.ENTITIES, vf.createLiteral(conceptNumber));
		graph.add(collectionPartition, VOID.CLASS, SKOS.COLLECTION);
		graph.add(collectionPartition, VOID.ENTITIES, vf.createLiteral(collectionNumber));
		graph.add(conceptSchemePartition, VOID.CLASS, SKOS.CONCEPT_SCHEME);
		graph.add(conceptSchemePartition, VOID.ENTITIES, vf.createLiteral(conceptSchemeNumber));

	}

}

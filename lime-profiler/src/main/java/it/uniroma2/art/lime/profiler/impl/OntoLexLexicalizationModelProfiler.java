package it.uniroma2.art.lime.profiler.impl;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.profiler.ProfilerOptions;

public class OntoLexLexicalizationModelProfiler extends AbstractLexicalizationModelProfiler {

	public static final IRI ONTOLEX_LEXICALIZATION_MODEL = SimpleValueFactory.getInstance()
			.createIRI("http://www.w3.org/ns/lemon/ontolex");

	@Override
	public IRI getLexicalizationModel() {
		return ONTOLEX_LEXICALIZATION_MODEL;
	}

	@Override
	protected TupleQuery prepareQuery(ProfilerOptions options,
			LIMERepositoryConnectionWrapper metadataConnection, RepositoryConnection dataConnection,
			IRI dataGraph) {
		StringBuilder sb = new StringBuilder(
				// @formatter:off
				" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>                             \n" +
				" PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>                            \n" +
				" PREFIX lime: <http://www.w3.org/ns/lemon/lime#>                                  \n" +
				" SELECT ?referenceDataset ?referenceDatasetUriSpace ?lexiconDataset ?lang         \n" +
				"        (COUNT(DISTINCT ?reference) as ?references)                               \n" +
				"        (COUNT(?lexicalEntry) as ?lexicalizations)                                \n" +
				"        (COUNT(DISTINCT ?lexicalEntry) as ?lexicalEntries) {                      \n" +
				"   {SELECT DISTINCT ?lexicalEntry ?reference (LANG(?label) as ?lang) {            \n" +
				"      GRAPH " + RenderUtils.toSPARQL(dataGraph) + " {                             \n" +
				"        ?lexicalEntry                                                             \n" +
				"        	(ontolex:sense|^ontolex:isSenseOf)                                     \n" +
				"        		/(ontolex:reference|^ontolex:isReferenceOf)                        \n" +
				"        	|(ontolex:denotes|^ontolex:isDenotedBy)                                \n" +
				"        			?reference .                                                   \n" +
				"        ?lexicalEntry ontolex:canonicalForm/ontolex:writtenRep ?label			   \n" +
				"      }                                                                           \n" +
				"   }}                                                                             \n" +
				"   GRAPH " + RenderUtils.toSPARQL(dataGraph) + " {                                \n" +			
				"     ?lexiconDataset lime:entry ?lexicalEntry .                                   \n"
				// @formatter:on
				);
		
		ResourceLocationUtilsInternal.appendUriSpaceLogic(options, dataGraph, sb, metadataConnection, "", "reference");

		sb.append(
			// @formatter:off
			"   }                                                                              \n" +
			" }                                                                                \n" +
			" GROUP BY ?referenceDataset ?referenceDatasetUriSpace ?lang ?lexiconDataset       \n"
			// @formatter:on
		);

		TupleQuery query = dataConnection.prepareTupleQuery(sb.toString());
		
		return query;
	}

	@Override
	protected boolean requiresLexicon() {
		return true;
	}

}

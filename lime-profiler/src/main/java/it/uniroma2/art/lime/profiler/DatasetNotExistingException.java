package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.Resource;

public class DatasetNotExistingException extends ProfilerException {

	private static final long serialVersionUID = 49012841546638164L;

	public DatasetNotExistingException(Resource dataset) {
		super("Dataset not existing: " + dataset.stringValue());
	}

}

package it.uniroma2.art.lime.profiler.impl;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.profiler.ProfilerContext;
import it.uniroma2.art.lime.profiler.ProfilerOptions;
import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.VOID;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ResourceLocationUtilsNew {
	public static Map<String, Resource> retrieveDatasetsByUriSpace(ProfilerOptions options,
			LIMERepositoryConnectionWrapper metadataConnection) {

		try (RepositoryResult<Statement> repositoryResult = metadataConnection.getStatements(null, VOID.URI_SPACE, null,
				options.isIncludeInferred(), options.getContexts());
			 Stream<Statement> stream = QueryResults.stream(repositoryResult)) {
			return stream.collect(Collectors.toMap(s -> s.getObject().stringValue(), Statement::getSubject, (v1, v2) -> v1));
		}
	}

	public static void appendUriSpaceLogic(ProfilerOptions options, IRI dataGraph, StringBuilder sb,
			LIMERepositoryConnectionWrapper metadataConnection, String varPrefix, String referenceVar) {
		Map<String, Resource> datasetsByUriSpace = retrieveDatasetsByUriSpace(options, metadataConnection);

		if (!datasetsByUriSpace.isEmpty() || !options.isDefaultToLocalReference()) {
			if (!datasetsByUriSpace.isEmpty()) { // write the bindings table only if not empty
				sb.append(
					// @formatter:off
					" OPTIONAL {                                                               \n" +
					" 	{VALUES(?" + varPrefix + "referenceDataset ?" + varPrefix + "referenceDatasetUriSpaceT) { \n"
					// @formatter:on
				);

				for (Map.Entry<String, Resource> entry : datasetsByUriSpace.entrySet()) {
					sb.append("(").append(NTriplesUtil.toNTriplesString(entry.getValue())).append(" ")
							.append(NTriplesUtil.toNTriplesString(
									SimpleValueFactory.getInstance().createLiteral(entry.getKey())))
							.append(")");
				}
				sb.append(
					// @formatter:off
					"   }}\n" +
					"   FILTER(STRSTARTS(STR(?" + referenceVar + "), ?" + varPrefix + "referenceDatasetUriSpaceT))                   \n" +
					" }                                                                                                              \n"
					//@formatter:on
				);
			}

			sb.append(
				// @formatter:off
				"  bind(IF(BOUND(?" + varPrefix + "referenceDatasetUriSpaceT), ?" + varPrefix +"referenceDatasetUriSpaceT,        \n" +
				"  			IF(not exists {graph "+ RenderUtils.toSPARQL(dataGraph)+" {?" + referenceVar + " a []}},              \n" +
				"  				REPLACE(STR(?" + referenceVar + "), \"(.+(#|\\\\/)).*\", \"$1\"),                                 \n" +
				"  					?unboundVariable)) as ?" + varPrefix + "referenceDatasetUriSpace)                             \n"
				// @formatter:on
			);
		}
	}

	public static Resource getDatasetOrMintNew(ProfilerContext profilerContext, Map<String, Resource> additionalDatasets,
			Resource mainDataset, IRI resourceDataset, String uriSpace) {
		if (resourceDataset != null) {
			return resourceDataset;
		} else {
			if (uriSpace == null) {
				return mainDataset;
			} else {
				Resource newDataset = additionalDatasets.get(uriSpace);

				if (newDataset == null) {
					newDataset = profilerContext.mintDatasetResource();
					additionalDatasets.put(uriSpace, newDataset);
				}

				return newDataset;
			}
		}
	}

}

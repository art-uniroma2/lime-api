package it.uniroma2.art.lime.profiler;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import it.uniroma2.art.lime.profiler.ProfilerOptions.ResourceGenerationMode;
import it.uniroma2.art.lime.profiler.junit.support.TestCase;

/**
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class LIMEProfilerTest {

	@Rule
	public TestCase testCase = new TestCase();

	private LIMEProfiler limeProfiler;

	@Before
	public void setup() {
		limeProfiler = new LIMEProfiler(testCase.getMetadataRepoConnection(),
				SimpleValueFactory.getInstance().createIRI("http://example.org/void.ttl"),
				testCase.getDataRepoConnection(),
				testCase.getDataRepoConnection().getValueFactory().createIRI("http://example.org/"));
	}

	@Test
	public void testLinkset() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setDefaultToLocalReference(false);
		options.setResourceGenerationMode(ResourceGenerationMode.BNODE);
		limeProfiler.profile(options);
	}

	@Test
	public void testLinkset_withSlashEndingResources() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setDefaultToLocalReference(false);
		options.setResourceGenerationMode(ResourceGenerationMode.BNODE);
		limeProfiler.profile(options);
	}

	@Test
	public void testLinkset_withGivenIdentifiers() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setDefaultToLocalReference(false);
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testOntologyOntoLexConceptualization() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testOntologyOntoLexLexicalization() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	// @Test
	// public void testOntologyOntoLexLexicalizationThirdPartyLexicon() throws Exception {
	// throw new UnsupportedOperationException("Scenario to be addressed");
	// }

	@Test
	public void testOntologyOntoLexRedundancy() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testOntologyRDFSLexicalization() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testOntologySKOSLexicalization() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testOntologySKOSXLLexicalization() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testProfileOntologyConceptualDataset() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testProfileOntologyConceptualDataset_withGivenIdentifiers() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testProfileThesaurusConceptualDataset() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testThirdPartyOntologyRDFSLexicalization() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testThirdPartyOntologyRDFSLexicalization_withManuallyDescribedOntology() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}

	@Test
	public void testProfileMultiwordnet() throws Exception {
		ProfilerOptions options = new ProfilerOptions();
		options.setMainDatasetName("TestReferenceDataset");
		limeProfiler.profile(options);
	}
}

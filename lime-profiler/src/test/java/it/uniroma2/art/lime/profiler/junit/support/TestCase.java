package it.uniroma2.art.lime.profiler.junit.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.RDFInserter;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.Assert;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.collect.Sets;

public class TestCase implements TestRule {
	private Repository dataRepo;
	private RepositoryConnection dataRepoConnection;

	private Repository metadataRepo;
	private RepositoryConnection metadataRepoConnection;

	@Override
	public Statement apply(Statement base, Description description) {
		return new Statement() {

			@Override
			public void evaluate() throws Throwable {
				dataRepo = null;
				dataRepoConnection = null;

				metadataRepo = null;
				metadataRepoConnection = null;

				dataRepo = new SailRepository(new MemoryStore());
				metadataRepo = new SailRepository(new MemoryStore());

				try {
					dataRepo.init();
					metadataRepo.init();

					String methodName = description.getMethodName();
					Class<?> testClass = description.getTestClass();

					String testCaseSpec = testClass.getSimpleName() + "-" + methodName + ".xml";

					DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document testCaseDoc;

					try (InputStream is = Objects.requireNonNull(testClass.getResourceAsStream(testCaseSpec),
							"missing input data for test " + testCaseSpec)) {
						testCaseDoc = docBuilder.parse(is);
					}

					Element testCaseElement = testCaseDoc.getDocumentElement();
					String baseURI = testCaseElement.getElementsByTagName("BaseURI").item(0).getTextContent();
					String inputDatasetText = testCaseElement.getElementsByTagName("InputDataset").item(0)
							.getTextContent();
					String inputMetadataText = testCaseElement.getElementsByTagName("InputMetadata").item(0)
							.getTextContent();
					String expectedMetadataText = testCaseElement.getElementsByTagName("ExpectedMetadata").item(0)
							.getTextContent();

					dataRepoConnection = dataRepo.getConnection();

					try {
						RDFParser rdfParser = Rio.createParser(RDFFormat.TRIG);
						rdfParser.setRDFHandler(new RDFInserter(dataRepoConnection));
						rdfParser.parse(new StringReader(inputDatasetText), baseURI);
					} catch (RDFParseException | RepositoryException | IOException e) {
						throw new RuntimeException(e);
					}

					metadataRepoConnection = metadataRepo.getConnection();
					try {
						RDFParser rdfParser = Rio.createParser(RDFFormat.TRIG);
						rdfParser.setRDFHandler(new RDFInserter(metadataRepoConnection));
						rdfParser.parse(new StringReader(inputMetadataText), baseURI);
					} catch (RDFParseException | RepositoryException | IOException e) {
						throw new RuntimeException(e);
					}
					
					base.evaluate();
					
					Model actualMetadata = new LinkedHashModel();
					
					metadataRepoConnection.export(new StatementCollector(actualMetadata));
					
					Model expectedMetadata = Rio.parse(new StringReader(expectedMetadataText), "http://example.org", RDFFormat.TRIG);
					
					if (!Models.isomorphic(expectedMetadata, actualMetadata)) {
						System.out.println("Test name: " + testClass.getSimpleName() + "." + methodName);

						System.out.println("-- expected metadata (" + expectedMetadata.size() + ") --");
						Rio.write(expectedMetadata, System.out, RDFFormat.TRIG, new WriterConfig().set(BasicWriterSettings.PRETTY_PRINT, true));

						System.out.println("-- actual metadata (" + actualMetadata.size() +") --");
						Rio.write(actualMetadata, System.out, RDFFormat.TRIG, new WriterConfig().set(BasicWriterSettings.PRETTY_PRINT, true));
						
						System.out.println("-- Actual - Expected --");
						System.out.println("+++");
						Rio.write(Sets.difference(Sets.newHashSet(actualMetadata), Sets.newHashSet(expectedMetadata)), System.out, RDFFormat.TRIG, new WriterConfig().set(BasicWriterSettings.PRETTY_PRINT, true));

						System.out.println("-- Actual - Expected --");
						System.out.println("--");
						Rio.write(Sets.difference(Sets.newHashSet(expectedMetadata), Sets.newHashSet(actualMetadata)), System.out, RDFFormat.TRIG, new WriterConfig().set(BasicWriterSettings.PRETTY_PRINT, true));

						Assert.fail("Metadata does not match");
					}
					
				} finally {
					try {
						if (dataRepoConnection != null) {
							dataRepoConnection.close();
						}
					} finally {
						try {
							if (dataRepo != null) {
								dataRepo.shutDown();
							}
						} finally {
							try {
								if (metadataRepoConnection != null) {
									metadataRepoConnection.close();
								}
							} finally {
								if (metadataRepo != null) {
									metadataRepo.shutDown();
								}
							}

						}
					}
				}
			}
		};

	}

	public Repository getDataRepo() {
		return dataRepo;
	}

	public RepositoryConnection getDataRepoConnection() {
		return dataRepoConnection;
	}

	public Repository getMetadataRepo() {
		return metadataRepo;
	}

	public RepositoryConnection getMetadataRepoConnection() {
		return metadataRepoConnection;
	}
}

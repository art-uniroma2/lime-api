package it.uniroma2.art.lime.profiler.junit.support;

import junitparams.converters.ConversionFailedException;
import junitparams.converters.Converter;
import junitparams.converters.Param;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;

public class String2IRIConverter implements Converter<Param, IRI> {
    @Override
    public void initialize(Param s) {
    }

    @Override
    public IRI convert(Object o) throws ConversionFailedException {
        return Values.iri(o.toString());
    }
}

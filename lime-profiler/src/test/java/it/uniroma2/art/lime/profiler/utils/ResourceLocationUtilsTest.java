package it.uniroma2.art.lime.profiler.utils;

import it.uniroma2.art.lime.profiler.junit.support.String2IRIConverter;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.converters.Param;
import org.eclipse.rdf4j.model.IRI;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

/**
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
@RunWith(JUnitParamsRunner.class)
public class ResourceLocationUtilsTest {

    public static String[] namespaceFromMentionTestCases() {
        return new String[] {
                "http://example.org/Person, http://example.org/",
                "http://example.org/Person/, http://example.org/",
                "http://example.org/a/b/Person, http://example.org/a/b/",
                "http://example.org/a/b/Person/, http://example.org/a/b/",
                "http://example.org#Person, http://example.org#",
                "http://example.org/a/b#Person, http://example.org/a/b#",
        };
    }

    @Parameters(method = "namespaceFromMentionTestCases")
    @Test
    public void testResourceMentionDecompositionPattern(
            @Param(converter = String2IRIConverter.class) IRI resource, String expectedNamespace) {
        var resourceAsString = resource.stringValue();

        assertThat(resourceAsString, matchesPattern(ResourceLocationUtils.RESOURCE_MENTION_NAMESPACE_PATTERN));
        var matcher = ResourceLocationUtils.RESOURCE_MENTION_NAMESPACE_PATTERN.matcher(resourceAsString);

        if (matcher.matches()) {
            assertThat(matcher.group(ResourceLocationUtils.NS_GROUP_INDEX), equalTo(expectedNamespace));
        }
    }

    @Parameters(method = "namespaceFromMentionTestCases")
    @Test
    public void testGuessNamespaceForResourceMention(
            @Param(converter = String2IRIConverter.class) IRI resource,
            String expectedNamespace) {
        assertThat(ResourceLocationUtils.guessNamespaceForResourceMention(resource), equalTo(expectedNamespace));
    }

}

package it.uniroma2.art.lime.model.vocabulary;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class AbstractModifiedVocabularyTest {

	private String modifiedVocabName;

	public AbstractModifiedVocabularyTest(String modifiedVocabName) {
		this.modifiedVocabName = modifiedVocabName;
	}

	@Test
	public void testModifiedVocabulary() throws IOException {
		String originalVocabName = modifiedVocabName.replaceAll("(.*)(\\..*)", "$1-orig$2");
	
		String modifiedVocabContent = IOUtils
				.toString(this.getClass().getResourceAsStream(modifiedVocabName), StandardCharsets.UTF_8);
		String originalVocabContent = IOUtils
				.toString(this.getClass().getResourceAsStream(originalVocabName), StandardCharsets.UTF_8);
	
		assertFalse(modifiedVocabContent.contains("<!DOCTYPE"));
		assertTrue(originalVocabContent.contains("<!DOCTYPE"));
	
		Model modifiedVocab = Rio.parse(new StringReader(modifiedVocabContent), "",
				Rio.getParserFormatForFileName(modifiedVocabName)
						.orElseThrow(() -> new IOException("Unrecognized file format")));
		Model originalVocab = Rio.parse(new StringReader(originalVocabContent), "",
				Rio.getParserFormatForFileName(originalVocabName)
						.orElseThrow(() -> new IOException("Unrecognized file format")));
	
		assertTrue(Models.isomorphic(originalVocab, modifiedVocab));
	
	}

}
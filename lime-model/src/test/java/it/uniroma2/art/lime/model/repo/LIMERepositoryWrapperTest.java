package it.uniroma2.art.lime.model.repo;
import java.util.Collection;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.Test;

import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.model.repo.LIMERepositoryWrapper;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

public class LIMERepositoryWrapperTest {
	@Test
	public void testLIMERepositoryWrapper() {
		Repository repository = new SailRepository(new MemoryStore());
		repository.init();
		try {
			LIMERepositoryWrapper repoWrapper = new LIMERepositoryWrapper(repository);

			ValueFactory vf = SimpleValueFactory.getInstance();

			IRI testSubj = vf.createIRI("http://testSubj");
			IRI testPred = vf.createIRI("http://testPred");
			IRI testObj = vf.createIRI("http://testObj");

			try (LIMERepositoryConnectionWrapper connWrapper = repoWrapper.getConnection()) {
				connWrapper.add(testSubj, testPred, testObj);
			}

			try (RepositoryConnection conn = repository.getConnection()) {
				Collection<Statement> statements = new LinkedHashModel();
				conn.export(new StatementCollector(statements));
				assertThat(statements,
						contains(conn.getValueFactory().createStatement(testSubj, testPred, testObj)));
			}
		} finally {
			repository.shutDown();
		}
	}
}

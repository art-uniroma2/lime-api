package it.uniroma2.art.lime.model.repo;

import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.avgAmbiguity;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.concepts;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.conceptualDataset;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.greaterThan;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.greaterThanOrEqualTo;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.hasProperty;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.language;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.lessThan;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.lexicalEntries;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.lexiconDataset;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.percentage;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.referenceDataset;
import static it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers.suchThat;

import java.util.Collection;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.contains;

import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LIMERepositoryConnectionWrapperTest {

	public LIMERepositoryWrapper repo;

	@Before
	public void setup() {
		repo = new LIMERepositoryWrapper(new SailRepository(new MemoryStore()));
		repo.init();
	}

	@After
	public void teardown() {
		if (repo != null) {
			repo.shutDown();
		}
	}

	@Test
	public void testGetLexicalizationSets() {
		IRI lexicalizationSet = SimpleValueFactory.getInstance()
				.createIRI("http://example.org/lexicalizationSetEn1");

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			conn.add(
				// @formatter:off
				new ModelBuilder().setNamespace("", "http://example.org/")
					.subject(":lexicalizationSetEn1")
						.add(RDF.TYPE, LIME.LEXICALIZATION_SET)
						.add(LIME.LANGUAGE, "en")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset1")
					.subject(":lexicalizationSetIt1")
						.add(RDF.TYPE, LIME.LEXICALIZATION_SET)
						.add(LIME.LANGUAGE, "it")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset1")
					.subject(":lexicalizationSetEn2")
						.add(RDF.TYPE, LIME.LEXICALIZATION_SET)
						.add(LIME.LANGUAGE, "en")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset2")
					.build()
				// @formatter:on
			);
		}

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			Collection<Resource> lexicalizationSets = QueryResults.asList(conn.getLexicalizationSets(
					suchThat(language("en"), referenceDataset("http://example.org/referenceDataset1"))));
			assertThat(lexicalizationSets, contains(lexicalizationSet));
		}
	}

	@Test
	public void testGetLexicalLinksets() {
		IRI lexicalLinkset = SimpleValueFactory.getInstance()
				.createIRI("http://example.org/lexicalLinkset1A");

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			conn.add(
				// @formatter:off
				new ModelBuilder().setNamespace("", "http://example.org/")
					.subject(":lexicalLinkset1A")
						.add(RDF.TYPE, LIME.LEXICAL_LINKSET)
						.add(LIME.CONCEPTUAL_DATASET, ":conceptSetA")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset1")
					.subject(":lexicalLinkset1B")
						.add(RDF.TYPE, LIME.LEXICAL_LINKSET)
						.add(LIME.CONCEPTUAL_DATASET, "conceptSetB")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset2")
					.subject(":lexicalLinkset2B")
						.add(RDF.TYPE, LIME.LEXICAL_LINKSET)
						.add(LIME.CONCEPTUAL_DATASET, "conceptSetB")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset2")
					.build()
				// @formatter:on
			);
		}

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			Collection<Resource> lexicalLinksets = QueryResults.asList(
					conn.getLexicalLinksets(suchThat(conceptualDataset("http://example.org/conceptSetA"),
							referenceDataset("http://example.org/referenceDataset1"))));
			assertThat(lexicalLinksets, contains(lexicalLinkset));
		}
	}

	@Test
	public void testGetLexicalLinksetsPercentageGreaterThan() {
		IRI lexicalLinkset = SimpleValueFactory.getInstance()
				.createIRI("http://example.org/lexicalLinkset1A");

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			conn.add(
				// @formatter:off
				new ModelBuilder().setNamespace("", "http://example.org/")
					.subject(":lexicalLinkset1A")
						.add(RDF.TYPE, LIME.LEXICAL_LINKSET)
						.add(LIME.CONCEPTUAL_DATASET, ":conceptSetA")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset1")
						.add(LIME.PERCENTAGE, 90.0)
					.subject(":lexicalLinkset1B")
						.add(RDF.TYPE, LIME.LEXICAL_LINKSET)
						.add(LIME.CONCEPTUAL_DATASET, "conceptSetA")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset1")
						.add(LIME.PERCENTAGE, 80.0)
					.subject(":lexicalLinkset2B")
						.add(RDF.TYPE, LIME.LEXICAL_LINKSET)
						.add(LIME.CONCEPTUAL_DATASET, "conceptSetB")
						.add(LIME.REFERENCE_DATASET, ":referenceDataset1")
					.build()
				// @formatter:on
			);
		}

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			Collection<Resource> lexicalLinksets = QueryResults.asList(
					conn.getLexicalLinksets(suchThat(conceptualDataset("http://example.org/conceptSetA"),
							referenceDataset("http://example.org/referenceDataset1"),
							percentage(greaterThanOrEqualTo(90.0)))));
			assertThat(lexicalLinksets, contains(lexicalLinkset));
		}
	}

	@Test
	public void testGetConceptualizationSetsComplex() {
		IRI conceptualizatioSet = SimpleValueFactory.getInstance()
				.createIRI("http://example.org/conceptualizationSet2");

		Model inputModel =
			// @formatter:off
			new ModelBuilder().setNamespace("", "http://example.org/")
				.subject(":conceptualizationSet1")
					.add(RDF.TYPE, LIME.CONCEPTUALIZATION_SET)
					.add(LIME.CONCEPTS, SimpleValueFactory.getInstance().createLiteral("10", XMLSchema.INTEGER))
					.add(LIME.LEXICAL_ENTRIES, SimpleValueFactory.getInstance().createLiteral("30", XMLSchema.INTEGER))
					.add(LIME.AVG_AMBIGUITY, SimpleValueFactory.getInstance().createLiteral("0.9", XMLSchema.DECIMAL))
				.subject(":conceptualizationSet2")
					.add(RDF.TYPE, LIME.CONCEPTUALIZATION_SET)
					.add(LIME.CONCEPTS, SimpleValueFactory.getInstance().createLiteral("10", XMLSchema.INTEGER))
					.add(LIME.LEXICAL_ENTRIES, SimpleValueFactory.getInstance().createLiteral("31", XMLSchema.INTEGER))
					.add(LIME.AVG_AMBIGUITY, SimpleValueFactory.getInstance().createLiteral("0.9", XMLSchema.DECIMAL))
				.subject(":conceptualizationSet3")
					.add(LIME.CONCEPTS, SimpleValueFactory.getInstance().createLiteral("10", XMLSchema.INTEGER))
					.add(LIME.LEXICAL_ENTRIES, SimpleValueFactory.getInstance().createLiteral("31", XMLSchema.INTEGER))
					.add(LIME.AVG_AMBIGUITY, SimpleValueFactory.getInstance().createLiteral("0.9", XMLSchema.DECIMAL))
				.build()
			// @formatter:on
		;

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			conn.add(inputModel);
		}

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			Collection<Resource> conceptualizationSets = QueryResults
					.asList(conn.getConceptualizationSets(suchThat(concepts(10),
							lexicalEntries(greaterThan(30)), avgAmbiguity(greaterThanOrEqualTo(0.9)))));

			assertThat(conceptualizationSets, contains(conceptualizatioSet));
		}
	}

	@Test
	public void testGetConceptualizationSetsNestedCondition() {
		IRI conceptualizationSet = SimpleValueFactory.getInstance()
				.createIRI("http://example.org/conceptualizationSet1");

		Model inputModel =
			// @formatter:off
			new ModelBuilder().setNamespace("", "http://example.org/").setNamespace("dbr", "http://dbpedia.org/resource/")
				.subject(":conceptualizationSet1")
					.add(RDF.TYPE, LIME.CONCEPTUALIZATION_SET)
					.add(LIME.AVG_AMBIGUITY, SimpleValueFactory.getInstance().createLiteral("0.4", XMLSchema.DECIMAL))
					.add(LIME.LEXICON_DATASET, ":lexicon1")
				.subject(":lexicon1")
					.add(LIME.LANGUAGE, "en")
					.add(DCTERMS.SUBJECT, "dbr:Agriculture")
				.build()
			// @formatter:on
		;

		Rio.write(inputModel, System.out, RDFFormat.TURTLE);
		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			conn.add(inputModel);
		}

		try (LIMERepositoryConnectionWrapper conn = repo.getConnection()) {
			Collection<Resource> conceptualizationSets = QueryResults
					.asList(conn.getConceptualizationSets(suchThat(avgAmbiguity(lessThan(0.5)),
							lexiconDataset(suchThat(language("en"),
									hasProperty(DCTERMS.SUBJECT, SimpleValueFactory.getInstance()
											.createIRI("http://dbpedia.org/resource/Agriculture")))))));

			assertThat(conceptualizationSets, contains(conceptualizationSet));
		}
	}

}

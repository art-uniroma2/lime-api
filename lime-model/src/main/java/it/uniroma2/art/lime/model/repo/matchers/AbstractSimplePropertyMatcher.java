package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.common.text.StringUtil;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;

public class AbstractSimplePropertyMatcher<T, S> implements Matcher<T> {
	private IRI property;
	private Value value;
	private Matcher<? super S> matcher;

	public AbstractSimplePropertyMatcher(IRI property, Value value) {
		this.property = property;
		this.value = value;
		this.matcher = null;
	}

	public AbstractSimplePropertyMatcher(IRI property, Matcher<? super S> matcher) {
		this.property = property;
		this.value = null;
		this.matcher = matcher;
	}

	@Override
	public void toSPARQL(StringBuilder sb, int padding, VariableFactory variableFactory, String varName) {
		StringUtil.appendN(' ', padding, sb);
		sb.append("?").append(varName).append(" ").append(NTriplesUtil.toNTriplesString(property))
				.append(" ");

		if (value != null) {
			sb.append(NTriplesUtil.toNTriplesString(value)).append(" .\n");
		} else {
			String objectVar = variableFactory.newVariableName();

			sb.append("?").append(objectVar).append(" .\n");

			matcher.toSPARQL(sb, padding, variableFactory, objectVar);
		}

	}

}

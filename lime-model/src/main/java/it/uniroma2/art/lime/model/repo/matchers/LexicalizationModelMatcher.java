package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.lime.model.classes.LexicalizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexicalizationModelMatcher
		extends AbstractSimplePropertyMatcher<LexicalizationSet, BigDecimal>
		implements Matcher<LexicalizationSet> {

	public LexicalizationModelMatcher(IRI lexicalizationModel) {
		super(LIME.LEXICALIZATION_MODEL, lexicalizationModel);
	}
}

package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.FilterIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import com.google.common.base.Objects;

public class DatatypeFilteredIteration<E extends Literal, X extends Exception> extends FilterIteration<E, X> {

	private IRI datatype;

	public DatatypeFilteredIteration(Iteration<? extends E, ? extends X> iter, IRI datatype) {
		super(iter);
		this.datatype = datatype;
	}

	@Override
	protected boolean accept(E object) throws X {
		return Objects.equal(object.getDatatype(), datatype);
	}

}

package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.lime.model.classes.ConceptSet;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class ConceptualDatasetMatcher
		extends AbstractSimplePropertyMatcher<ConceptualizationSetOrLexicalLinkset, ConceptSet>
		implements Matcher<ConceptualizationSetOrLexicalLinkset> {
	public ConceptualDatasetMatcher(IRI conceptualDataset) {
		super(LIME.CONCEPTUAL_DATASET, conceptualDataset);
	}

	public ConceptualDatasetMatcher(Matcher<? super ConceptSet> matcher) {
		super(LIME.CONCEPTUAL_DATASET, matcher);
	}
}

package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.AbstractCloseableIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.RepositoryException;

public class IRIIteration extends AbstractCloseableIteration<IRI, RepositoryException> {

	private Iteration<? extends Value, ? extends RepositoryException> iter;

	public IRIIteration(Iteration<? extends Value, ? extends RepositoryException> iter) {
		this.iter = new ClassFilteredIteration<>(iter, IRI.class);
	}

	@Override
	public boolean hasNext() throws RepositoryException {
		return iter.hasNext();
	}

	@Override
	public IRI next() throws RepositoryException {
		return (IRI) iter.next();
	}

	@Override
	public void remove() throws RepositoryException {
		iter.remove();
	}

}

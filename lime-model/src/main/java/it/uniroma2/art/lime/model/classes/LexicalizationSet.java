package it.uniroma2.art.lime.model.classes;

import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicalizationSet;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicalizationSet;
import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicon;

public interface LexicalizationSet extends Dataset, ConceptSetOrLexicalizationSet, ConceptualizationSetOrLexicalizationSet, LexicalizationSetOrLexicalLinkset, LexicalizationSetOrLexicon {

}

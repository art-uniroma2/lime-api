package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicalLinksetOrConceptualizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class ConceptsMatcher extends AbstractSimplePropertyMatcher<ConceptSetOrLexicalLinksetOrConceptualizationSet, BigInteger>
		implements Matcher<ConceptSetOrLexicalLinksetOrConceptualizationSet> {

	public ConceptsMatcher(BigInteger lexicalEntries) {
		super(LIME.CONCEPTS, SimpleValueFactory.getInstance().createLiteral(lexicalEntries));
	}
	
	public ConceptsMatcher(Matcher<BigInteger> matcher) {
		super(LIME.CONCEPTS, matcher);
	}
}

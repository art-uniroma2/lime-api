package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.common.text.StringUtil;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;

public class ComparisonMatcher<T> implements Matcher<T> {
	public static enum Relation {
		LT, LTE, GTE, GT
	}

	private Literal comparisonTerm;
	private Relation rel;

	public ComparisonMatcher(Relation rel, Literal comparisonTerm) {
		this.rel = rel;
		this.comparisonTerm = comparisonTerm;
	}

	@Override
	public void toSPARQL(StringBuilder sb, int padding, VariableFactory variableFactory, String varName) {
		StringUtil.appendN(' ', padding, sb);
		sb.append("FILTER( ?").append(varName).append(" ").append(getOperatorCharacter()).append(" ")
				.append(NTriplesUtil.toNTriplesString(comparisonTerm)).append(")\n");
	}

	private String getOperatorCharacter() {
		switch (rel) {
		case LT:
			return "<";
		case LTE:
			return "<=";
		case GTE:
			return ">=";
		case GT:
			return ">";
		}
		throw new IllegalStateException("Unrecognized relation: " + rel);
	}

}

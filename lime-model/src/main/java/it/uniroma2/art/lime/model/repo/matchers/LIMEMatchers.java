package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.ConceptSet;
import it.uniroma2.art.lime.model.classes.Dataset;
import it.uniroma2.art.lime.model.classes.Lexicon;

public abstract class LIMEMatchers {
	@SafeVarargs
	public static <T> Matcher<T> suchThat(Matcher<? super T>... matchers) {
		return new MatcherConjunction<T>(Arrays.asList(matchers));
	}

	public static AvgAmbiguityMatcher avgAmbiguity(double avgAmbiguity) {
		return avgAmbiguity(BigDecimal.valueOf(avgAmbiguity));
	}

	public static AvgAmbiguityMatcher avgAmbiguity(BigDecimal avgAmbiguity) {
		return new AvgAmbiguityMatcher(avgAmbiguity);
	}

	public static AvgAmbiguityMatcher avgAmbiguity(Matcher<BigDecimal> matcher) {
		return new AvgAmbiguityMatcher(matcher);
	}

	public static AvgNumOfLinksMatcher avgNumOfLinks(double avgNumOfLinks) {
		return avgNumOfLinks(BigDecimal.valueOf(avgNumOfLinks));
	}

	public static AvgNumOfLinksMatcher avgNumOfLinks(BigDecimal avgNumOfLinks) {
		return new AvgNumOfLinksMatcher(avgNumOfLinks);
	}

	public static AvgNumOfLinksMatcher avgNumOfLinks(Matcher<BigDecimal> matcher) {
		return new AvgNumOfLinksMatcher(matcher);
	}

	public static AvgNumOfLexicalizationsMatcher avgNumOfLexicalizations(BigDecimal percentage) {
		return new AvgNumOfLexicalizationsMatcher(percentage);
	}

	public static AvgNumOfLexicalizationsMatcher avgNumOfLexicalizations(Matcher<BigDecimal> matcher) {
		return new AvgNumOfLexicalizationsMatcher(matcher);
	}

	public static AvgSynonymyMatcher avgSynonymy(double avgSynonymy) {
		return avgSynonymy(BigDecimal.valueOf(avgSynonymy));
	}

	public static AvgSynonymyMatcher avgSynonymy(BigDecimal avgSynonymy) {
		return new AvgSynonymyMatcher(avgSynonymy);
	}

	public static AvgSynonymyMatcher avgSynonymy(Matcher<BigDecimal> matcher) {
		return new AvgSynonymyMatcher(matcher);
	}

	public static ConceptsMatcher concepts(long concepts) {
		return concepts(BigInteger.valueOf(concepts));
	}

	public static ConceptsMatcher concepts(BigInteger concepts) {
		return new ConceptsMatcher(concepts);
	}

	public static ConceptsMatcher concepts(Matcher<BigInteger> matcher) {
		return new ConceptsMatcher(matcher);
	}

	public static ConceptualDatasetMatcher conceptualDataset(String iriString) {
		return conceptualDataset(SimpleValueFactory.getInstance().createIRI(iriString));
	}

	public static ConceptualDatasetMatcher conceptualDataset(IRI iri) {
		return new ConceptualDatasetMatcher(iri);
	}
	
	public static ConceptualDatasetMatcher conceptualDataset(Matcher<? super ConceptSet> matcher) {
		return new ConceptualDatasetMatcher(matcher);
	}

	public static LanguageMatcher language(String lang) {
		return new LanguageMatcher(lang);
	}

	public static LexicalizationModelMatcher lexicalizationModel(String iriString) {
		return lexicalizationModel(SimpleValueFactory.getInstance().createIRI(iriString));
	}

	public static LexicalizationModelMatcher lexicalizationModel(IRI iri) {
		return new LexicalizationModelMatcher(iri);
	}

	public static LexicalEntriesMatcher lexicalEntries(long lexicalEntries) {
		return lexicalEntries(BigInteger.valueOf(lexicalEntries));
	}

	public static LexicalEntriesMatcher lexicalEntries(BigInteger lexicalEntries) {
		return new LexicalEntriesMatcher(lexicalEntries);
	}

	public static LexicalEntriesMatcher lexicalEntries(Matcher<BigInteger> matcher) {
		return new LexicalEntriesMatcher(matcher);
	}

	public static LexicalizationsMatcher lexicalizations(long lexicalizations) {
		return lexicalizations(BigInteger.valueOf(lexicalizations));
	}

	public static LexicalizationsMatcher lexicalizations(BigInteger lexicalizations) {
		return new LexicalizationsMatcher(lexicalizations);
	}

	public static LexicalizationsMatcher lexicalizations(Matcher<BigInteger> matcher) {
		return new LexicalizationsMatcher(matcher);
	}

	public static LexiconDatasetMatcher lexiconDataset(String iriString) {
		return lexiconDataset(SimpleValueFactory.getInstance().createIRI(iriString));
	}

	public static LexiconDatasetMatcher lexiconDataset(IRI iri) {
		return new LexiconDatasetMatcher(iri);
	}

	public static LexiconDatasetMatcher lexiconDataset(Matcher<? super Lexicon> matcher) {
		return new LexiconDatasetMatcher(matcher);
	}
	
	public static LinguisticCatalogMatcher linguisticCatalog(String iriString) {
		return linguisticCatalog(SimpleValueFactory.getInstance().createIRI(iriString));
	}

	public static LinguisticCatalogMatcher linguisticCatalog(IRI iri) {
		return new LinguisticCatalogMatcher(iri);
	}

	public static LinksMatcher links(long links) {
		return links(BigInteger.valueOf(links));
	}

	public static LinksMatcher links(BigInteger links) {
		return new LinksMatcher(links);
	}

	public static LinksMatcher links(Matcher<BigInteger> matcher) {
		return new LinksMatcher(matcher);
	}

	public static PercentageMatcher percentage(float percentage) {
		return new PercentageMatcher(BigDecimal.valueOf(percentage));
	}

	public static PercentageMatcher percentage(BigDecimal percentage) {
		return new PercentageMatcher(percentage);
	}

	public static PercentageMatcher percentage(Matcher<BigDecimal> matcher) {
		return new PercentageMatcher(matcher);
	}

	public static ReferenceDatasetMatcher referenceDataset(String iriString) {
		return referenceDataset(SimpleValueFactory.getInstance().createIRI(iriString));
	}

	public static ReferenceDatasetMatcher referenceDataset(IRI iri) {
		return new ReferenceDatasetMatcher(iri);
	}
	
	public static ReferenceDatasetMatcher referenceDataset(Matcher<? super Dataset> matcher) {
		return new ReferenceDatasetMatcher(matcher);
	}

	public static ReferencesMatcher references(long references) {
		return new ReferencesMatcher(BigInteger.valueOf(references));
	}

	public static ReferencesMatcher references(BigInteger references) {
		return new ReferencesMatcher(references);
	}

	public static ReferencesMatcher references(Matcher<BigInteger> matcher) {
		return new ReferencesMatcher(matcher);
	}

	public static Matcher<BigInteger> lessThan(long comparisonTerm) {
		return lessThan(BigInteger.valueOf(comparisonTerm));
	}

	public static Matcher<BigInteger> lessThan(BigInteger comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.LT,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}
	
	public static Matcher<BigInteger> lessThanOrEqualTo(long comparisonTerm) {
		return lessThanOrEqualTo(BigInteger.valueOf(comparisonTerm));
	}

	public static Matcher<BigInteger> lessThanOrEqualTo(BigInteger comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.LTE,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}
	
	public static Matcher<BigInteger> greaterThanOrEqualTo(long comparisonTerm) {
		return greaterThanOrEqualTo(BigInteger.valueOf(comparisonTerm));
	}

	public static Matcher<BigInteger> greaterThanOrEqualTo(BigInteger comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.GTE,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}

	public static Matcher<BigInteger> greaterThan(long comparisonTerm) {
		return greaterThan(BigInteger.valueOf(comparisonTerm));
	}

	public static Matcher<BigInteger> greaterThan(BigInteger comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.GT,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}
	
	public static Matcher<BigDecimal> lessThan(double comparisonTerm) {
		return lessThan(BigDecimal.valueOf(comparisonTerm));
	}

	public static Matcher<BigDecimal> lessThan(BigDecimal comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.LT,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}
	
	public static Matcher<BigDecimal> lessThanOrEqualTo(double comparisonTerm) {
		return lessThanOrEqualTo(BigDecimal.valueOf(comparisonTerm));
	}

	public static Matcher<BigDecimal> lessThanOrEqualTo(BigDecimal comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.LTE,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}
	
	public static Matcher<BigDecimal> greaterThanOrEqualTo(double comparisonTerm) {
		return greaterThanOrEqualTo(BigDecimal.valueOf(comparisonTerm));
	}

	public static Matcher<BigDecimal> greaterThanOrEqualTo(BigDecimal comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.GTE,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}

	public static Matcher<BigDecimal> greaterThan(double comparisonTerm) {
		return greaterThan(BigDecimal.valueOf(comparisonTerm));
	}

	public static Matcher<BigDecimal> greaterThan(BigDecimal comparisonTerm) {
		return new ComparisonMatcher<>(ComparisonMatcher.Relation.GT,
				SimpleValueFactory.getInstance().createLiteral(comparisonTerm));
	}

	public static PropertyValueMatcher hasProperty(IRI property, Value value) {
		return new PropertyValueMatcher(property, value);
	}
}

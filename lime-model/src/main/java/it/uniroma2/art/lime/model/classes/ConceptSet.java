package it.uniroma2.art.lime.model.classes;

import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrConceptualizationSet;
import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicalizationSet;
import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicon;

public interface ConceptSet extends Dataset, ConceptSetOrConceptualizationSet, ConceptSetOrLexicalizationSet,
		ConceptSetOrLexicalLinkset, ConceptSetOrLexicon {

}

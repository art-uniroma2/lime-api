package it.uniroma2.art.lime.model.utils;

import java.math.BigInteger;

import org.eclipse.rdf4j.common.iteration.AbstractCloseableIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.repository.RepositoryException;

public class BigIntegerIteration extends AbstractCloseableIteration<BigInteger, RepositoryException> {

	private Iteration<? extends Literal, ? extends RepositoryException> iter;

	public BigIntegerIteration(Iteration<? extends Value, ? extends RepositoryException> iter) {
		this.iter = new DatatypeFilteredIteration<>(new LiteralIteration(iter), XMLSchema.INTEGER);
	}

	@Override
	public boolean hasNext() throws RepositoryException {
		return iter.hasNext();
	}

	@Override
	public BigInteger next() throws RepositoryException {
		return Literals.getIntegerValue(iter.next(), BigInteger.ZERO);
	}

	@Override
	public void remove() throws RepositoryException {
		iter.remove();
	}

}

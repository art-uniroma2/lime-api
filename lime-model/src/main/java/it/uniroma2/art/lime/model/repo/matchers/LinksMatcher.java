package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.LexicalLinkset;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LinksMatcher extends AbstractSimplePropertyMatcher<LexicalLinkset, BigInteger>
		implements Matcher<LexicalLinkset> {

	public LinksMatcher(BigInteger percentage) {
		super(LIME.LINKS, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public LinksMatcher(Matcher<BigInteger> matcher) {
		super(LIME.LINKS, matcher);
	}
}

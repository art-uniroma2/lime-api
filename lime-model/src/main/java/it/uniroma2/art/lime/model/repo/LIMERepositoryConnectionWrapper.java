package it.uniroma2.art.lime.model.repo;

import java.math.BigInteger;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.VOID;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.base.RepositoryConnectionWrapper;

import it.uniroma2.art.lime.model.classes.ConceptSet;
import it.uniroma2.art.lime.model.classes.ConceptualizationSet;
import it.uniroma2.art.lime.model.classes.Dataset;
import it.uniroma2.art.lime.model.classes.LexicalLinkset;
import it.uniroma2.art.lime.model.classes.LexicalizationSet;
import it.uniroma2.art.lime.model.classes.Lexicon;
import it.uniroma2.art.lime.model.classes.Vocabulary;
import it.uniroma2.art.lime.model.repo.matchers.LIMEMatchers;
import it.uniroma2.art.lime.model.repo.matchers.Matcher;
import it.uniroma2.art.lime.model.repo.matchers.VariableFactory;
import it.uniroma2.art.lime.model.utils.BigIntegerIteration;
import it.uniroma2.art.lime.model.utils.BindingSetProjectionIteration;
import it.uniroma2.art.lime.model.utils.IRIIteration;
import it.uniroma2.art.lime.model.utils.LiteralIteration;
import it.uniroma2.art.lime.model.utils.ObjectIteration;
import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;
import it.uniroma2.art.lime.model.vocabulary.VOAF;

public class LIMERepositoryConnectionWrapper extends RepositoryConnectionWrapper {

	public LIMERepositoryConnectionWrapper(Repository repository) {
		super(repository);
	}

	public LIMERepositoryConnectionWrapper(Repository repository, RepositoryConnection delegate) {
		super(repository, delegate);
	}

	private <T> RepositoryResult<Resource> getResources(IRI resourceClass, Matcher<? super T> pattern,
			Resource... contexts) {
		return getResources(resourceClass, pattern, true, contexts);
	}

	private <T> RepositoryResult<Resource> getResources(IRI resourceClass, Matcher<? super T> pattern,
			boolean includInferred, Resource... contexts) {
		VariableFactory variableFactory = new VariableFactory();

		String mainVarName = variableFactory.newVariableName();
		int padding = 2;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ?").append(mainVarName).append(" {\n");

		LIMEMatchers.hasProperty(RDF.TYPE, resourceClass).toSPARQL(sb, padding, variableFactory, mainVarName);
		pattern.toSPARQL(sb, padding, variableFactory, mainVarName);

		sb.append("}\n");

		TupleQuery query = getDelegate().prepareTupleQuery(sb.toString());
		return new RepositoryResult<>(
				new BindingSetProjectionIteration<>(query.evaluate(), Resource.class, mainVarName));
	}

	public RepositoryResult<Resource> getConceptualizationSets(Matcher<? super ConceptualizationSet> pattern,
			Resource... contexts) throws RepositoryException {
		return getResources(LIME.CONCEPTUALIZATION_SET, pattern, contexts);
	}

	public RepositoryResult<Resource> getConceptualizationSets(Matcher<? super ConceptualizationSet> pattern,
			boolean includeInferred, Resource... contexts) throws RepositoryException {
		return getResources(LIME.CONCEPTUALIZATION_SET, pattern, includeInferred, contexts);
	}

	public RepositoryResult<Resource> getConceptSets(Matcher<? super ConceptSet> pattern,
			Resource... contexts) throws RepositoryException {
		return getResources(ONTOLEX.CONCEPT_SET, pattern, contexts);
	}

	public RepositoryResult<Resource> getConceptSets(Matcher<? super ConceptSet> pattern,
			boolean includeInferred, Resource... contexts) throws RepositoryException {
		return getResources(ONTOLEX.CONCEPT_SET, pattern, includeInferred, contexts);
	}

	public RepositoryResult<Resource> getDatasets(Matcher<? super Dataset> pattern, boolean includeInferred,
			Resource... contexts) throws RepositoryException {
		return getResources(VOID.DATASET, pattern, includeInferred, contexts);
	}

	public RepositoryResult<Resource> getDatasets(Matcher<? super Dataset> pattern, Resource... contexts)
			throws RepositoryException {
		return getResources(VOID.DATASET, pattern, contexts);
	}

	public RepositoryResult<Resource> getLexicalizationSets(Matcher<? super LexicalizationSet> pattern,
			Resource... contexts) throws RepositoryException {
		return getResources(LIME.LEXICALIZATION_SET, pattern, contexts);
	}

	public RepositoryResult<Resource> getLexicalizationSets(Matcher<? super LexicalizationSet> pattern,
			boolean includeInferred, Resource... contexts) throws RepositoryException {
		return getResources(LIME.LEXICALIZATION_SET, pattern, includeInferred, contexts);
	}

	public RepositoryResult<Resource> getLexicalLinksets(Matcher<? super LexicalLinkset> pattern,
			Resource... contexts) throws RepositoryException {
		return getResources(LIME.LEXICAL_LINKSET, pattern, contexts);
	}

	public RepositoryResult<Resource> getLexicons(Matcher<? super Lexicon> pattern, boolean includeInferred,
			Resource... contexts) throws RepositoryException {
		return getResources(LIME.LEXICON, pattern, includeInferred, contexts);
	}

	public RepositoryResult<Resource> getLexicons(Matcher<? super Lexicon> pattern, Resource... contexts)
			throws RepositoryException {
		return getResources(LIME.LEXICON, pattern, contexts);
	}

	public RepositoryResult<Value> getProperties(Resource resource, IRI property, boolean includeInferred,
			Resource... contexts) throws RepositoryException {
		return new RepositoryResult<>(
				new ObjectIteration(getStatements(resource, property, null, includeInferred, contexts)));
	}

	public RepositoryResult<Literal> getPropertyStrings(Resource resource, IRI property,
			boolean includeInferred, Resource... contexts) {
		return new RepositoryResult<>(new LiteralIteration(
				new ObjectIteration(getStatements(resource, property, null, includeInferred, contexts))));
	}

	public RepositoryResult<BigInteger> getPropertyIntegers(Resource resource, IRI property, boolean includeInferred,
			Resource... contexts) {
		return new RepositoryResult<>(new BigIntegerIteration(
				new ObjectIteration(getStatements(resource, property, null, includeInferred, contexts))));
	}

	
	public RepositoryResult<IRI> getPropertyIRIs(Resource resource, IRI property, boolean includeInferred,
			Resource... contexts) {
		return new RepositoryResult<>(new IRIIteration(
				new ObjectIteration(getStatements(resource, property, null, includeInferred, contexts))));
	}

	public RepositoryResult<Resource> getVocabularies(Matcher<? super Vocabulary> pattern,
			boolean includeInferred, Resource... contexts) throws RepositoryException {
		return getResources(VOAF.VOCABULARY, pattern, includeInferred, contexts);
	}

	public RepositoryResult<Resource> getVocabularies(Matcher<? super Vocabulary> pattern,
			Resource... contexts) throws RepositoryException {
		return getResources(VOAF.VOCABULARY, pattern, contexts);
	}

	public Optional<Resource> getMainDataset(boolean includeInferred, Resource... contexts)
			throws RepositoryException {
		try (RepositoryResult<Value> repositoryResult = getProperties(null, FOAF.PRIMARY_TOPIC, includeInferred, contexts);
			 Stream<Value> stream = QueryResults.stream(repositoryResult)) {
			return stream.filter(Resource.class::isInstance).map(Resource.class::cast).findAny();
		}
	}
}

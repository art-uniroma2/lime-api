package it.uniroma2.art.lime.model.classes;

import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrConceptualizationSet;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicalizationSet;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicon;

public interface ConceptualizationSet
		extends Dataset, ConceptSetOrConceptualizationSet, ConceptualizationSetOrLexicalizationSet,
		ConceptualizationSetOrLexicalLinkset, ConceptualizationSetOrLexicon {

}

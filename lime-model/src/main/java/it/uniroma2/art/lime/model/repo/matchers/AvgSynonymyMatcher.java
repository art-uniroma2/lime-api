package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.ConceptualizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class AvgSynonymyMatcher extends AbstractSimplePropertyMatcher<ConceptualizationSet, BigDecimal>
		implements Matcher<ConceptualizationSet> {

	public AvgSynonymyMatcher(BigDecimal percentage) {
		super(LIME.AVG_SYNONYMY, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public AvgSynonymyMatcher(Matcher<BigDecimal> matcher) {
		super(LIME.AVG_SYNONYMY, matcher);
	}
}

package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.FilterIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;

public class ClassFilteredIteration<E, X extends Exception> extends FilterIteration<E, X> {

	private Class<?> clazz;

	public ClassFilteredIteration(Iteration<? extends E, ? extends X> iter, Class<?> clazz) {
		super(iter);
		this.clazz = clazz;
	}

	@Override
	protected boolean accept(E object) throws X {
		return clazz.isInstance(object);
	}

}

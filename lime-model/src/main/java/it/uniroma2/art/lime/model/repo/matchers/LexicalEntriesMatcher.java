package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexiconOrLexicalizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexicalEntriesMatcher extends AbstractSimplePropertyMatcher<ConceptualizationSetOrLexiconOrLexicalizationSet, BigInteger>
		implements Matcher<ConceptualizationSetOrLexiconOrLexicalizationSet> {

	public LexicalEntriesMatcher(BigInteger lexicalEntries) {
		super(LIME.LEXICAL_ENTRIES, SimpleValueFactory.getInstance().createLiteral(lexicalEntries));
	}
	
	public LexicalEntriesMatcher(Matcher<BigInteger> matcher) {
		super(LIME.LEXICAL_ENTRIES, matcher);
	}
}

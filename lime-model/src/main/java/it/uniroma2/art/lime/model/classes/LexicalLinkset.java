package it.uniroma2.art.lime.model.classes;

import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.classes.lattice.LexicalLinksetOrLexicon;
import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicalLinkset;

public interface LexicalLinkset extends Dataset, ConceptSetOrLexicalLinkset,
		ConceptualizationSetOrLexicalLinkset, LexicalizationSetOrLexicalLinkset, LexicalLinksetOrLexicon {

}

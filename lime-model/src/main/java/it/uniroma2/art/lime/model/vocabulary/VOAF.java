package it.uniroma2.art.lime.model.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

/**
 * Constants for the Vocabulary of a Friend (VOAF) Vocabulary.
 *
 * @see <a href="https://www.w3.org/2016/05/ontolex/#metadata-lime">Vocabulary of a Friend (VOAF)</a>
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class VOAF {

	/**
	 * The VOAF namespace: http://purl.org/vocommons/voaf#
	 */
	public static final String NAMESPACE = "http://purl.org/vocommons/voaf#";

	/**
	 * Recommended prefix for the Vocabulary of a Friend (VOAF) namespace: "voaf"
	 */
	public static final String PREFIX = "voaf";

	/**
	 * An immutable {@link Namespace} constant that represents the Vocabulary of a Friend (VOAF) namespace.
	 */
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	// Classes

	/** voaf:DatasetOccurrences */
	public static final IRI DATASET_OCCURRENCES;

	/** voaf:Vocabulary */
	public static final IRI VOCABULARY;

	/** voaf:VocabularySpace */
	public static final IRI VOCABULARY_SPACE;

	// Properties

	/** voaf:classNumber */
	public static final IRI CLASS_NUMBER;

	/** voaf:dataset */
	public static final IRI DATASET;

	/** voaf:exampleDataset */
	public static final IRI EXAMPLE_DATASET;

	/** voaf:extends */
	public static final IRI EXTENDS;

	/** voaf:generalizes */
	public static final IRI GENERALIZES;

	/** voaf:hasDisjunctionsWith */
	public static final IRI HAS_DISJUNCTIONS_WITH;

	/** voaf:hasEquivalencesWith */
	public static final IRI HAS_EQUIVALENCES_WITH;

	/** voaf:inDataset */
	public static final IRI IN_DATASET;

	/** voaf:metadataVoc */
	public static final IRI METADATA_VOC;

	/** voaf:occurrences */
	public static final IRI OCCURRENCES;

	/** voaf:occurrencesInDatasets */
	public static final IRI OCCURRENCES_IN_DATASETS;

	/** voaf:occurrencesInVocabularies */
	public static final IRI OCCURRENCES_IN_VOCABULARIES;

	/** voaf:propertyNumber */
	public static final IRI PROPERTY_NUMBER;

	/** voaf:reliesOn */
	public static final IRI RELIES_ON;

	/** voaf:reusedByDatasets */
	public static final IRI REUSED_BY_DATASETS;

	/** voaf:reusedByVocabularies */
	public static final IRI REUSED_BY_VOCABULARIES;

	/** voaf:similar */
	public static final IRI SIMILAR;

	/** voaf:specializes */
	public static final IRI SPECIALIZES;

	/** voaf:toDoList */
	public static final IRI TO_DO_LIST;

	/** voaf:usageInDataset */
	public static final IRI USAGE_IN_DATASET;

	/** voaf:usedBy */
	public static final IRI USED_BY;

	static {
		ValueFactory vf = SimpleValueFactory.getInstance();

		DATASET_OCCURRENCES = vf.createIRI(NAMESPACE, "DatasetOccurrences");
		VOCABULARY = vf.createIRI(NAMESPACE, "Vocabulary");
		VOCABULARY_SPACE = vf.createIRI(NAMESPACE, "VocabularySpace");

		CLASS_NUMBER = vf.createIRI(NAMESPACE, "classNumber");
		DATASET = vf.createIRI(NAMESPACE, "dataset");
		EXAMPLE_DATASET = vf.createIRI(NAMESPACE, "exampleDataset");
		EXTENDS = vf.createIRI(NAMESPACE, "extends");
		GENERALIZES = vf.createIRI(NAMESPACE, "generalizes");
		HAS_DISJUNCTIONS_WITH = vf.createIRI(NAMESPACE, "hasDisjunctionsWith");
		HAS_EQUIVALENCES_WITH = vf.createIRI(NAMESPACE, "hasEquivalencesWith");
		IN_DATASET = vf.createIRI(NAMESPACE, "inDataset");
		METADATA_VOC = vf.createIRI(NAMESPACE, "metadataVoc");
		OCCURRENCES = vf.createIRI(NAMESPACE, "occurrences");
		OCCURRENCES_IN_DATASETS = vf.createIRI(NAMESPACE, "occurrencesInDatasets");
		OCCURRENCES_IN_VOCABULARIES = vf.createIRI(NAMESPACE, "occurrencesInVocabularies");
		PROPERTY_NUMBER = vf.createIRI(NAMESPACE, "propertyNumber");
		RELIES_ON = vf.createIRI(NAMESPACE, "reliesOn");
		REUSED_BY_DATASETS = vf.createIRI(NAMESPACE, "reusedByDatasets");
		REUSED_BY_VOCABULARIES = vf.createIRI(NAMESPACE, "reusedByVocabularies");
		SIMILAR = vf.createIRI(NAMESPACE, "similar");
		SPECIALIZES = vf.createIRI(NAMESPACE, "specializes");
		TO_DO_LIST = vf.createIRI(NAMESPACE, "toDoList");
		USAGE_IN_DATASET = vf.createIRI(NAMESPACE, "usageInDataset");
		USED_BY = vf.createIRI(NAMESPACE, "usedBy");
	}
}
package it.uniroma2.art.lime.model.utils;

import java.util.function.Function;
import java.util.stream.Stream;

public class CastFlatMapper<S, T> implements Function<S, Stream<T>>{

	private Class<T> clazz;

	public CastFlatMapper(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	@Override
	public Stream<T> apply(S t) {
		if (clazz.isInstance(t)) {
			return Stream.of(clazz.cast(t));
		} else {
			return Stream.empty();
		}
	}
	
}

package it.uniroma2.art.lime.model.classes;

import it.uniroma2.art.lime.model.classes.lattice.ConceptSetOrLexicon;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicon;
import it.uniroma2.art.lime.model.classes.lattice.LexicalLinksetOrLexicon;
import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicon;

public interface Lexicon extends Dataset, ConceptSetOrLexicon, ConceptualizationSetOrLexicon, LexicalizationSetOrLexicon, LexicalLinksetOrLexicon {

}

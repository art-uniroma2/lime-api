package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.LexicalizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexicalizationsMatcher extends AbstractSimplePropertyMatcher<LexicalizationSet, BigInteger>
		implements Matcher<LexicalizationSet> {

	public LexicalizationsMatcher(BigInteger lexicalizations) {
		super(LIME.LEXICALIZATIONS, SimpleValueFactory.getInstance().createLiteral(lexicalizations));
	}

	public LexicalizationsMatcher(Matcher<BigInteger> matcher) {
		super(LIME.LEXICALIZATIONS, matcher);
	}
}

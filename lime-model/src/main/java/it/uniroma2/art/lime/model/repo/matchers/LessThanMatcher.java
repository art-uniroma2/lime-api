package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.common.text.StringUtil;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;

public class LessThanMatcher<T> implements Matcher<T> {
	private Literal upperBound;

	public LessThanMatcher(Literal upperBound) {
		this.upperBound = upperBound;
	}

	@Override
	public void toSPARQL(StringBuilder sb, int padding, VariableFactory variableFactory, String varName) {
		StringUtil.appendN(' ', padding, sb);
		sb.append("FILTER( ?").append(varName).append(" < ").append(NTriplesUtil.toNTriplesString(upperBound))
				.append(")\n");
	}

}

package it.uniroma2.art.lime.model.repo.matchers;

public interface Matcher<T> {
	 void toSPARQL(StringBuilder sb, int padding, VariableFactory variableFactory, String varName);
}

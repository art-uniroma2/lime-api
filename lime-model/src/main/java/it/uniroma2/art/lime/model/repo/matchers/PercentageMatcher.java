package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class PercentageMatcher extends AbstractSimplePropertyMatcher<LexicalizationSetOrLexicalLinkset, BigDecimal>
		implements Matcher<LexicalizationSetOrLexicalLinkset> {

	public PercentageMatcher(BigDecimal percentage) {
		super(LIME.PERCENTAGE, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public PercentageMatcher(Matcher<BigDecimal> matcher) {
		super(LIME.PERCENTAGE, matcher);
	}
}

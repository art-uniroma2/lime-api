package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.LexicalLinkset;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class AvgNumOfLinksMatcher extends AbstractSimplePropertyMatcher<LexicalLinkset, BigDecimal>
		implements Matcher<LexicalLinkset> {

	public AvgNumOfLinksMatcher(BigDecimal percentage) {
		super(LIME.AVG_NUM_OF_LINKS, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public AvgNumOfLinksMatcher(Matcher<BigDecimal> matcher) {
		super(LIME.AVG_NUM_OF_LINKS, matcher);
	}
}

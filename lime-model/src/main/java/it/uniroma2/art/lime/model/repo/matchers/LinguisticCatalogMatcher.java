package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.lime.model.classes.Lexicon;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LinguisticCatalogMatcher extends AbstractSimplePropertyMatcher<Lexicon, Void>
		implements Matcher<Lexicon> {

	public LinguisticCatalogMatcher(IRI linguisticCatalog) {
		super(LIME.LINGUISTIC_CATALOG, linguisticCatalog);
	}
}

package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.ConvertingIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.repository.RepositoryException;

public class SubjectIteration extends ConvertingIteration<Statement, Resource, RepositoryException>{

	public SubjectIteration(Iteration<? extends Statement, ? extends RepositoryException> iter) {
		super(iter);
	}

	@Override
	protected Resource convert(Statement sourceObject) throws RepositoryException {
		return sourceObject.getSubject();
	}

}

package it.uniroma2.art.lime.model.language;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class LanguageTagUtils {
	private static Map<String, IRI> tag2lexvo = new HashMap<>();
	private static Map<String, String> iso639_32iso639_1 = new HashMap<>();
	private static Map<String, String> iso639_2_52iso639_1 = new HashMap<>();
	private static Set<String> bcp47languages = new HashSet<>();
	static {
		try {
			ingestLexvoMapping("lexvo-iso639-1.tsv");
			ingestLexvoMapping("lexvo-iso639-2.tsv");
			ingestLexvoMapping("lexvo-iso639-3.tsv");
			ingestISO639tables();
			ingestBCP47languages();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void ingestLexvoMapping(String mappingName) throws IOException {
		ValueFactory vf = SimpleValueFactory.getInstance();

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(LanguageTagUtils.class.getResourceAsStream(mappingName)))) {
			String line;
			while ((line = reader.readLine()) != null) {
				String[] components = line.split("\\t");

				if (components.length != 2)
					continue;

				tag2lexvo.put(components[0], vf.createIRI(components[1]));
			}

		}
	}

	private static void ingestBCP47languages() throws IOException {
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(LanguageTagUtils.class.getResourceAsStream("language.json")))) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(reader);
			jsonNode.fieldNames().forEachRemaining(bcp47languages::add);
		}
	}

	private static void ingestISO639tables() throws IOException {

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(LanguageTagUtils.class.getResourceAsStream("iso639-codes.csv")))) {
			int lineNumber = 1;
			reader.readLine(); // skip header
			// 0=639-3
			// 1=639-2/639-5 (optionally ISO (639-2/T)/(ISO 639-2/B))
			// 2=639-1
			// 3=Language Name
			// 4=Scope
			// 5=Type
			String line;
			while ((line = reader.readLine()) != null) {
				lineNumber++;
				String[] components = line.split(";", -1);
				if (components.length != 6) {
					System.out.println("components = " + Arrays.toString(components));
					throw new IOException("Illegal number of components on line " + lineNumber);
				}

				String iso639_3 = components[0].trim();
				String iso639_2_5;
				String iso639_2_5_other;
				int separatorPosition = components[1].indexOf('/');
				if (separatorPosition == -1) {
					iso639_2_5 = empty2null(components[1]);
					iso639_2_5_other = null;
				} else {
					iso639_2_5 = empty2null(components[1].substring(0, separatorPosition));
					iso639_2_5_other = empty2null(components[1].substring(separatorPosition + 1));
				}
				String iso639_1 = empty2null(components[2]);

				if (iso639_1 != null) {
					if (iso639_3 != null) {
						iso639_32iso639_1.put(iso639_3, iso639_1);
					}

					if (iso639_2_5 != null) {
						iso639_2_52iso639_1.put(iso639_2_5, iso639_1);
					}

					if (iso639_2_5_other != null) {
						iso639_2_52iso639_1.put(iso639_2_5_other, iso639_1);
					}
				}
			}
		}

	}

	private static String empty2null(String value) {
		value = value.trim();
		if (value.isEmpty()) {
			return null;
		} else {
			return value;
		}
	}

	public static Optional<String> toISO639_1(String languageTag) {
		if (languageTag.length() == 2) {
			return Optional.of(languageTag); // already an ISO 639-1 code
		}
		String rv = iso639_32iso639_1.get(languageTag);
		if (rv != null) {
			return Optional.of(rv);
		}

		return Optional.ofNullable(iso639_2_52iso639_1.get(languageTag));
	}

	public static Optional<IRI> toLexvo(String languageTag) {
		return Optional.ofNullable(tag2lexvo.get(languageTag));
	}

	public static Optional<IRI> toLOC(String languageTag) {
		return toISO639_1(languageTag).map(tag -> SimpleValueFactory.getInstance()
				.createIRI("http://id.loc.gov/vocabulary/iso639-1/" + tag));
	}

	public static Optional<String> toBCP47(String languageTag) {
		if (bcp47languages.contains(languageTag))
			return Optional.of(languageTag);

		return toISO639_1(languageTag).filter(bcp47languages::contains);
	}
}

package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.common.text.StringUtil;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;

import it.uniroma2.art.lime.model.classes.RdfResource;

public class PropertyValueMatcher implements Matcher<RdfResource> {
	private IRI property;
	private Value value;

	public PropertyValueMatcher(IRI property, Value value) {
		this.property = property;
		this.value = value;
	}

	@Override
	public void toSPARQL(StringBuilder sb, int padding, VariableFactory variableFactory, String varName) {
		StringUtil.appendN(' ', padding, sb);
		sb.append("?").append(varName).append(" ").append(NTriplesUtil.toNTriplesString(property))
				.append(" ").append(NTriplesUtil
						.toNTriplesString(value)).append(" .\n");
	}

}

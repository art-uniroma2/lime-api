package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.LexicalizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class AvgNumOfLexicalizationsMatcher extends AbstractSimplePropertyMatcher<LexicalizationSet, BigDecimal>
		implements Matcher<LexicalizationSet> {

	public AvgNumOfLexicalizationsMatcher(BigDecimal percentage) {
		super(LIME.AVG_NUM_OF_LEXICALIZATIONS, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public AvgNumOfLexicalizationsMatcher(Matcher<BigDecimal> matcher) {
		super(LIME.AVG_NUM_OF_LEXICALIZATIONS, matcher);
	}
}

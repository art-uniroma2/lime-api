package it.uniroma2.art.lime.model.repo.matchers;

import java.util.LinkedList;
import java.util.List;

public class MatcherConjunction<T> implements Matcher<T>{
	private LinkedList<Matcher<? super T>> matchers;

	public MatcherConjunction(List<Matcher<? super T>> matchers) {
		this.matchers = new LinkedList<>(matchers);
	}

	public MatcherConjunction(MatcherConjunction<? super T> pattern) {
		this.matchers = new LinkedList<>();
		this.matchers.addAll(pattern.matchers);
	}

	public void appendMatcher(Matcher<? super T> matcher) {
		this.matchers.add(matcher);
	}

	public void prependMatcher(Matcher<? super T> matcher) {
		this.matchers.addFirst(matcher);
	}

	@Override
	public void toSPARQL(StringBuilder sb, int padding, VariableFactory variableFactory, String mainVarName) {
		for (Matcher<? super T> m : matchers) {
			m.toSPARQL(sb, padding, variableFactory, mainVarName);
		}
	}
}

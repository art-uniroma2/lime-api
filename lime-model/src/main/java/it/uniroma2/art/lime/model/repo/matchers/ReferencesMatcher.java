package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigInteger;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class ReferencesMatcher extends AbstractSimplePropertyMatcher<LexicalizationSetOrLexicalLinkset, BigInteger>
		implements Matcher<LexicalizationSetOrLexicalLinkset> {

	public ReferencesMatcher(BigInteger percentage) {
		super(LIME.REFERENCES, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public ReferencesMatcher(Matcher<BigInteger> matcher) {
		super(LIME.REFERENCES, matcher);
	}
}

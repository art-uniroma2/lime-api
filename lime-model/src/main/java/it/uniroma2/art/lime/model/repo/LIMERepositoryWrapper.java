package it.uniroma2.art.lime.model.repo;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.base.RepositoryWrapper;

public class LIMERepositoryWrapper extends RepositoryWrapper {

	public LIMERepositoryWrapper() {
	}

	public LIMERepositoryWrapper(Repository delegate) {
		super(delegate);
	}

	@Override
	public LIMERepositoryConnectionWrapper getConnection() throws RepositoryException {
		return new LIMERepositoryConnectionWrapper(this, super.getConnection());
	}
}

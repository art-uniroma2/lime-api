package it.uniroma2.art.lime.model.repo.matchers;

import java.math.BigDecimal;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.ConceptualizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class AvgAmbiguityMatcher extends AbstractSimplePropertyMatcher<ConceptualizationSet, BigDecimal>
		implements Matcher<ConceptualizationSet> {

	public AvgAmbiguityMatcher(BigDecimal percentage) {
		super(LIME.AVG_AMBIGUITY, SimpleValueFactory.getInstance().createLiteral(percentage));
	}
	
	public AvgAmbiguityMatcher(Matcher<BigDecimal> matcher) {
		super(LIME.AVG_AMBIGUITY, matcher);
	}
}

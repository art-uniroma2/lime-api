package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.AbstractCloseableIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.RepositoryException;

public class LiteralIteration extends AbstractCloseableIteration<Literal, RepositoryException> {

	private Iteration<? extends Value, ? extends RepositoryException> iter;

	public LiteralIteration(Iteration<? extends Value, ? extends RepositoryException> iter) {
		this.iter = new ClassFilteredIteration<>(iter, Literal.class);
	}
	
	@Override
	public boolean hasNext() throws RepositoryException {
		return iter.hasNext();
	}

	@Override
	public Literal next() throws RepositoryException {
		return (Literal)iter.next();
	}

	@Override
	public void remove() throws RepositoryException {
		iter.remove();
	}


}

package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.ConvertingIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.RepositoryException;

public class ObjectIteration extends ConvertingIteration<Statement, Value, RepositoryException> {

	public ObjectIteration(Iteration<? extends Statement, ? extends RepositoryException> iter) {
		super(iter);
	}

	@Override
	protected Value convert(Statement sourceObject) throws RepositoryException {
		return sourceObject.getObject();
	}

}

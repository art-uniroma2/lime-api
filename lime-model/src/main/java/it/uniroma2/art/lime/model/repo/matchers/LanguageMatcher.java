package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicon;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LanguageMatcher extends AbstractSimplePropertyMatcher<LexicalizationSetOrLexicon, Void>
		implements Matcher<LexicalizationSetOrLexicon> {

	public LanguageMatcher(String langTag) {
		super(LIME.LANGUAGE, SimpleValueFactory.getInstance().createLiteral(langTag));
	}

}

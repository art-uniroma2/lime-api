package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.lime.model.classes.Lexicon;
import it.uniroma2.art.lime.model.classes.lattice.ConceptualizationSetOrLexicalizationSet;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class LexiconDatasetMatcher
		extends AbstractSimplePropertyMatcher<ConceptualizationSetOrLexicalizationSet, Lexicon>
		implements Matcher<ConceptualizationSetOrLexicalizationSet> {

	public LexiconDatasetMatcher(IRI lexiconDataset) {
		super(LIME.LEXICON_DATASET, lexiconDataset);
	}

	public LexiconDatasetMatcher(Matcher<? super Lexicon> matcher) {
		super(LIME.LEXICON_DATASET, matcher);
	}
}

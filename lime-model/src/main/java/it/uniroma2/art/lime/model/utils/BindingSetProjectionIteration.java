package it.uniroma2.art.lime.model.utils;

import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.repository.RepositoryException;

public class BindingSetProjectionIteration<Q, T extends BindingSet> implements CloseableIteration<Q, RepositoryException>{

	private CloseableIteration<T, QueryEvaluationException> iter;
	private Class<Q> clazz;
	private String varName;

	public BindingSetProjectionIteration(CloseableIteration<T, QueryEvaluationException> iter, Class<Q> clazz, String varName) {
		this.iter = iter;
		this.clazz = clazz;
		this.varName = varName;
	}

	@Override
	public boolean hasNext() throws RepositoryException {
		try {
			return iter.hasNext();
		} catch(QueryEvaluationException e) {
			throw new RepositoryException(e);
		}
	}

	@Override
	public Q next() throws RepositoryException {
		try {
			return clazz.cast(iter.next().getValue(varName));
		} catch(QueryEvaluationException e) {
			throw new RepositoryException(e);
		}
	}

	@Override
	public void remove() throws RepositoryException {
		try {
			iter.remove();
		} catch(QueryEvaluationException e) {
			throw new RepositoryException(e);
		}
	}

	@Override
	public void close() throws RepositoryException {
		try {
			iter.close();
		} catch(QueryEvaluationException e) {
			throw new RepositoryException(e);
		}
	}

}

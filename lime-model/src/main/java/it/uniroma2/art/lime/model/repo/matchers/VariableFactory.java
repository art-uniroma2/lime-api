package it.uniroma2.art.lime.model.repo.matchers;

public class VariableFactory {
	private int count;
	
	public String newVariableName() {
		return "v" + (count++);
	}
	
	public void reset() {
		count = 0;
	}
}

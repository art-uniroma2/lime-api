package it.uniroma2.art.lime.model.repo.matchers;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.lime.model.classes.Dataset;
import it.uniroma2.art.lime.model.classes.lattice.LexicalizationSetOrLexicalLinkset;
import it.uniroma2.art.lime.model.vocabulary.LIME;

public class ReferenceDatasetMatcher extends AbstractSimplePropertyMatcher<LexicalizationSetOrLexicalLinkset, Dataset>
		implements Matcher<LexicalizationSetOrLexicalLinkset> {

	public ReferenceDatasetMatcher(IRI referenceDataset) {
		super(LIME.REFERENCE_DATASET, referenceDataset);
	}

	public ReferenceDatasetMatcher(Matcher<? super Dataset> matcher) {
		super(LIME.REFERENCE_DATASET, matcher);
	}
}

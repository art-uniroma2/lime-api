LInguistic MEtadata (LIME) API
==============================

This project contains an API for working with metadata conforming to the [LIME vocabulary](http://www.w3.org/2016/05/ontolex/#metadata-lime), which is part of the
[Ontology Lexicon Model](https://www.w3.org/2016/05/ontolex/) (Lemon) defined by the [W3C OntoLex Community Group](http://www.w3.org/community/ontolex/).

Notes on embedded resources
---------------------------
This projects includes resources published by third-parties:
 * some mapping files [published](http://www.lexvo.org/linkeddata/resources.html) by Lexvo.org
 * ISO 639 code tables [published](http://www-01.sil.org/iso639-3/codes.asp?order=639_3&letter=%25#note) by SIL International
 * [BCP 47/IANA language subtag data in JSON format](https://github.com/mattcg/language-subtag-registry)

Bibliographic References
---------------------------
If you need to cite this work, these are two references to the LIME vocabulary and this LIME API respectively

__LIME Vocabulary:__

Fiorelli, M., Stellato, A., Mccrae, J. P., Cimiano, P., Pazienza, M. T. (2015) [LIME: the Metadata Module for OntoLex](https://dx.doi.org/10.1007/978-3-319-18818-8_20). In F. Gandon, M. Sabou, H. Sack, C. d’Amato, P. Cudré-Mauroux, & A. Zimmermann (Eds.) The Semantic Web. Latest Advances and New Domains. ESWC 2015. Lecture Notes in Computer Science, vol. 9088, pp. 321-336. Springer International Publishing. doi:10.1007/978-3-319-18818-8_20

__LIME API:__

Fiorelli, M., Pazienza, M. T., Stellato, A. (2017) [An API for OntoLex LIME datasets](http://ceur-ws.org/Vol-1899/#OntoLex_2017_paper_8). In Proceedings of the LDK 2017 Workshops: 1st Workshop on the OntoLex Model (OntoLex-2017), Shared Task on Translation Inference Across
Dictionaries & Challenges for Wordnets co-located with 1st Conference on Language, Data and Knowledge (LDK 2017), Galway, Ireland, June 18, 2017